<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespostasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respostas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_users')->unsigned();
            $table->foreign('id_users')->references('id')->on('users')->onDelete('cascade');
            $table->integer('id_solcitacao')->unsigned();
            $table->foreign('id_solicitacao')->references('id')->on('solicitacaos')->onDelete('cascade');
            $table->mediumText('resposta');
            $table->string('tipo_resposta');
            $table->string('prorrogar');
            $table->string('arquivo_resposta');
            $table->string('status');
            $table->string('adm');
            $table->string('data_fim');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respostas');
    }
}
