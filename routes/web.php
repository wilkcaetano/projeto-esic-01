<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|Wilk Caetano
*/


Route::group(['middleware' => 'web'], function (){
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home/registrar', 'UsersController@create')->name('registrar');
Route::get('/solicitar', 'SolicitacaoController@index')->name('solicitar');
Route::post('/solicitar', 'SolicitacaoController@store')->name('cadastrar');
Route::get('/acompanhar', 'SolicitacaoController@acompanhar')->name('acompanhar');
Route::get('/graficos', 'SolicitacaoController@graficos')->name('graficos');
Route::get('/user_tramitacao/{id}', 'SolicitacaoController@tramitacao')->name('user_tramitacao');
Route::get('chat/{id}', 'SolicitacaoController@chat')->name('resposta_chat');
Route::get('/', function () {
    return view('index');
});

});

Route::group(['middleware' => 'admin'], function (){
    Route::group(['middleware' => 'auth:admin'], function (){
        Route::get('/admin','AdminController@index')->name('home');
        Route::get('/admin/recentes','AdminController@recentes')->name('recentes');
        Route::get('/admin/abrir/{id}','SolicitacaoAdmController@abrir')->name('abrir');
        Route::get('/admin/respondendo/{id}','SolicitacaoAdmController@respondendo')->name('respondendo');
        Route::post('/admin/resposta/{id}', 'SolicitacaoAdmController@resposta')->name('resposta');
        Route::get('/admin/abertas', 'AdminController@abertas')->name('abertas');
        Route::get('/admin/respondidas', 'SolicitacaoAdmController@respondidas')->name('respondidas');
        Route::get('/admin/prorrogadas', 'SolicitacaoAdmController@prorrogar')->name('prorrogadas');
        Route::get('/admin/tramitacao/{id}', 'SolicitacaoAdmController@tramitacao')->name('tramitacao');
        Route::get('/admin/usuarios', 'AdminController@usuarios')->name('usuarios');
        Route::get('/admin/usuarios/{id}', 'ChatController@index')->name('chat');
        Route::post('/admin/usuarios', 'ChatController@store')->name('chatMsn');
        Route::get('/admin/usuarios/delt/{id}', 'AdminController@delete')->name('delete');
        Route::get('/admin/usuarios/destroy/{id}', 'AdminController@destroy')->name('destroy');
        Route::get('cadastro/adm', 'AdminController@cadastroadm')->name('cadastro.adm');
        Route::post('cadastro/adm', 'AdminController@createadm')->name('criaradm');
        Route::get('visualizar/adm', 'AdminController@view')->name('visualizar');
        Route::get('admin/mais-userfinal/{id}', 'AdminController@maisinformacoes')->name('infor-user');
        Route::get('admin/info-admin/{id}', 'AdminController@infoadmin')->name('info-adm');
        Route::get('admin/delet-admin/{id}', 'AdminController@viewdelet')->name('delet-adm');
        Route::get('admin/destroy-admin/{id}', 'AdminController@destroiadm')->name('destroy-adm');
    });



    Route::get('/admin/login','AdminController@login');
    Route::post('/admin/login','AdminController@postLogin');


    Route::post('/admin/logout', 'AdminController@logout')->name('logout-adm');

});
