<?php $__env->startSection('content'); ?>
    <div class="col-md-12" >
        <div class="container" style="margin-top: 80px;height: 100%;">

            <div class="col-md-12">

                <h3 class="title-5 m-b-35">lista de ADM's cadastrados no sistema</h3>

                <div class="table-responsive table-responsive-data2">

                    <table class="table table-data2">

                        <thead>

                        <tr>

                            <th>nome</th>

                            <th>email</th>

                            <th>cargo</th>

                            <th>data de cadastro</th>


                        </tr>

                        </thead>


                        <?php $__currentLoopData = $admin; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $admins): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tbody style="background: #edecec;">
                            <tr class="tr-shadow">
                                <td><?php echo e($admins->name); ?></td>
                                <td>
                                    <span class="block-email"><?php echo e($admins->email); ?></span>
                                </td>
                                <td class="desc"><?php echo e($admins->cargo); ?></td>
                                <td>
                                    <span class="status--process"><?php echo e(date('d/m/Y', strtotime($admins->created_at))); ?></span>
                                </td>
                                <td>
                                    <div class="table-data-feature">
                                        <a href="" class="item" data-toggle="tooltip" data-placement="top" title="Chat Mensagens Curtas">
                                            <i class="zmdi zmdi-mail-send"></i>
                                        </a>
                                        <a href="" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                        <a href="<?php echo e(route('info-adm', $admins->id)); ?>" class="item" data-toggle="tooltip" data-placement="top" title="Mais Informações">
                                            <i class="zmdi zmdi-more"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr class="spacer"></tr>
                            </tr>
                            </tbody>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>