
<style>
    #linha-cont{
        height: 30px;
        border-bottom: 1px solid #d4edda;
        background: #edecec;
        text-align: center;
        line-height: 30px;
    }
    #menor{
        position: relative;
        float: left;
    }

    #conteudo-linha{
        padding: 0;
    }
    #conteudo-linhas-menores{
        min-height: 35px;
        line-height: 35px;
        border: 1px solid #edecec;
        text-align: center;
        position: relative;
        float: left;
        font-size: 15px;
        color: blue;
    }
</style>
<?php $__env->startSection('content'); ?>
    <div class="col-md-12" style="height: 50px; text-align: center;line-height: 50px;font-size: 20px;background: firebrick;margin-top: 70px;color: white;">
        Tem Certeza que você deseja excluir <?php echo e($user->name); ?>  do sistema?
    </div>
    <div class="col-md-12" style="padding: 0;position: relative;float: left;margin-top: 20px;">
        <div class="col-md-12" id="linha-cont">
            <div class="col-md-4" id="menor">
                Nome do Solicitante
            </div>
            <div class="col-md-4" id="menor">
                Email do Solicitante
            </div>
            <div class="col-md-4" id="menor">
                Deletar
            </div>
        </div>
        <div class="col-md-12" id="conteudo-linha">
            <div class="col-md-4" id="conteudo-linhas-menores">
                <?php echo e($user->name); ?>

            </div>
            <div class="col-md-4" id="conteudo-linhas-menores">
                <?php echo e($user->email); ?>

            </div>
            <div class="col-md-4" id="conteudo-linhas-menores">
                <a href="<?php echo e(route('destroy', $user->id)); ?>">
                    <botton class="btn btn-danger">
                        Deletar
                    </botton>
                </a>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>