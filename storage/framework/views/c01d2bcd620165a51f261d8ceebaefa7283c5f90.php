

<?php $__env->startSection('content'); ?>
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">solicitações realizadas</h3>
        <div class="table-data__tool">
            <div class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead>
                    <tr>
                        <?php if($solicitacao->status != 'NR'): ?>
                            <th>Administrador da Resposta</th>
                            <th>Tipo de Resposta</th>
                            <th>mensagem</th>
                            <th>Data de Resposta</th>
                            <th>status</th>
                            <th>resposta</th>
                            <th>anexo do administrador</th>
                        <?php endif; ?>
                    </tr>
                    </thead>
                        <?php $__currentLoopData = $resposta; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $respostas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($respostas->id_solicitacao == $solicitacao->id): ?>
                            <tbody>
                            <tr class="tr-shadow">
                                <td>
                                    <?php echo e($respostas->adm); ?>

                                </td>
                                <td>
                                    <?php if($respostas->resposta == 'Aberto'): ?>
                                        <?php echo e("Não Consta"); ?>

                                    <?php else: ?>
                                        <?php echo e($respostas->tipo_resposta); ?>

                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php echo e($solicitacao->mensagem); ?>

                                </td>
                                <td class="desc">
                                    <?php if($respostas->resposta == 'Aberto'): ?>
                                        <?php echo e(date('m/d/Y', strtotime($respostas->created_at))); ?>

                                    <?php else: ?>
                                        <?php echo e(date('d/m/Y', strtotime($respostas->created_at))); ?>

                                    <?php endif; ?>
                                </td>
                                <td>
                                   <?php if($respostas->status == 'AR'): ?>
                                        <spam style="color: #00a2e3;">
                                            <?php echo e("Foi Aberta"); ?>

                                        </spam>
                                        <?php elseif($respostas->status == 'PR'): ?>
                                        <spam class="status--denied">
                                            <?php echo e("Foi Prorrogado"); ?>

                                        </spam>
                                       <?php elseif($respostas->status == 'RP'): ?>
                                        <spam class="status--process">
                                             <?php echo e("Foi Respondido"); ?>

                                        </spam>
                                   <?php endif; ?>
                                </td>
                                <td>
                                    <?php if($solicitacao->id == $respostas->id_solicitacao): ?>
                                        <?php if($respostas->resposta == 'Aberto'): ?>
                                            <?php echo e("Não Consta"); ?>

                                        <?php else: ?>
                                            <?php echo e($respostas->resposta); ?>

                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if(!empty($respostas->arquivo_resposta)): ?>
                                        <a href="<?php echo e(url('public/uploads/arquivos/respostas/'.$respostas->id_users.'/'.$respostas->arquivo_resposta)); ?>">Anexo</a>
                                        <?php else: ?>
                                        <?php echo e("Não Consta"); ?>

                                    <?php endif; ?>
                                </td>

                            </tr>
                            </tbody>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php if($solicitacao->status == 'NR'): ?>
                        <tbody>
                            <?php echo e("Não Consta Respostas"); ?>

                        </tbody>
                    <?php endif; ?>
                </table>
            </div>
            <!-- END DATA TABLE -->
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('painel.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>