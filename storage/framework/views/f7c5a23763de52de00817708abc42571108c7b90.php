

<?php $__env->startSection('content'); ?>
    <div class="col-md-12">
        <form action="<?php echo e(route('cadastrar')); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="container" style="background: white;padding: 0;">
                <div class="card-header">
                    <strong>Solicitar</strong>
                    <small> Informação</small>
                </div>
                <div class="card-body card-block">
                    <div class="form-group">
                        <label for="company" class=" form-control-label">Prioridade</label>
                        <select name="prioridade" id="" class="form-control">
                            <option value=""></option>
                            <option value="Baixa">Baixa</option>
                            <option value="Media">Media</option>
                            <option value="Alta">Alta</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="vat" class=" form-control-label">Tipo de Solicitação</label>
                        <select name="tipo" id="" class="form-control">
                            <option value=""></option>
                            <option value="Pedido de Infromação">Pedido de Infromação</option>
                            <option value="Elogio">Elogio</option>
                            <option value="Sujestões">Sujestões</option>
                            <option value="Denuncias">Denuncias</option>
                            <option value="Reclamações">Reclamações</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="street" class=" form-control-label">Mensagem</label>
                        <textarea name="mensagem" class="form-control" style="height: 250px;"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="file" name="arquivo">
                        <input type="hidden" name="id_users" value="<?php echo e(Auth::user()->id); ?>">
                        <input type="hidden" name="name" value="<?php echo e(Auth::user()->name); ?>">
                    </div>
                    <input class="btn btn-primary" type="submit" value="Enviar Solicitação" />
                </div>
            </div>
        </form>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('painel.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>