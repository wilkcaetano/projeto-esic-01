<?php $__env->startSection('content'); ?>

    <div class="col-md-12">
        <div class="col-md-12">
            <aside class="profile-nav alt">
                <section class="card">
                    <div class="card-header user-header alt " style="background: red;">
                        <div class="media">
                            <a href="#">
                                <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="<?php echo e(url('public/img/avatar-homem.jpg')); ?>">
                            </a>
                            <div class="media-body">
                                <h2 class="text-light display-6"><?php echo e($user->name); ?></h2>
                                <p style="color: white;">Tem Certeza Que Deseja excluir esse Usuário?</p>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <spam class="cor">Sexo:</spam> <?php echo e($user->sexo); ?>

                        </li>
                        <li class="list-group-item">
                            <spam class="cor">Profissão: </spam> <?php echo e($user->profissao); ?>

                        </li>
                        <li class="list-group-item">
                            <spam class="cor">Faixa Etária: </spam> <?php echo e($user->faixa_etaria); ?>

                        </li>
                        <li class="list-group-item">
                            <spam class="cor"> Escolaridade: </spam> <?php echo e($user->escolaridade); ?>

                        </li>
                        <li class="list-group-item">
                            <spam class="cor"> Rua: </spam> <?php echo e($user->rua); ?> nº: <?php echo e($user->numero); ?>

                        </li>
                        <li class="list-group-item">
                            <spam class="cor"> CEP: </spam> <?php echo e($user->cep); ?>

                        </li>
                        <li class="list-group-item">
                            <a href="javascript:history.back()" class="btn btn-primary">Voltar</a>
                            <a href="<?php echo e(route('destroy', $user->id)); ?>" class="btn btn-danger">
                                Deletar
                            </a>
                        </li>
                    </ul>

                </section>
            </aside>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>