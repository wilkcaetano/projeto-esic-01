<?php $__env->startSection('content'); ?>
    <div class="box" style="margin-top: 8%;">
        <div class="box-header">
            <h3 class="box-title">Solicitações Que Foram Prorrogadas!</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Estado</th>
                    <th>Nome Solicitante</th>
                    <th>Tipo</th>
                    <th>Data da Solicitação</th>
                    <th>Data Resposta</th>
                </tr>
                </thead>
                <?php $__currentLoopData = $solicitacao; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $solicitacoes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($user->id == $solicitacoes->id_users): ?>
                    <?php if($solicitacoes->status == 'PR'): ?>
                        <tbody>
                        <tr>
                            <td>
                                <a href="<?php echo e(route('abrir', $solicitacoes->id)); ?>">
                                    <img src="<?php echo e(asset('img/incorrect.png')); ?>" alt="" width="40">
                                </a>
                            </td>
                            <td>
                                <?php echo e($user->name); ?>

                            </td>
                            <td>
                                <?php echo e($solicitacoes->tipo); ?>

                            </td>
                            <td>
                                <?php echo e(date('d/m/Y', strtotime($solicitacoes->created_at))); ?>

                            </td>
                            <td>
                                <?php
                                $datacri = $solicitacoes->created_at;
                                echo date('d/m/y', strtotime($datacri.'+ 30 days'));
                                ?>
                            </td>
                        </tr>
                        </tbody>
                    <?php endif; ?>
                        <?php endif; ?>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>