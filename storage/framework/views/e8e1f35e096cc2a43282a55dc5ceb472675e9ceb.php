<?php $__env->startSection('content'); ?>
<div class="page-wrapper" >
    <div class="page-content" style="background: #edecec; min-height: 300px; overflow: hidden;">
        <div class="container" style="padding: 20px;">
            <div class="login-wrap" style="margin-top: -60px;">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="#">
                            <img src="<?php echo e(asset('img/esic_livre.png')); ?>" alt="CoolAdmin">
                        </a>
                    </div>
                    <div class="login-form">
                        <form method="POST" action="<?php echo e(route('registrar')); ?>" aria-label="<?php echo e(__('Register')); ?>">
                            <?php echo e(csrf_field()); ?>

                            <div class="form-group">
                                <label>Nome Completo</label>
                                <input id="name" type="text" class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" name="name" value="<?php echo e(old('name')); ?>" required autofocus>
                                <?php if($errors->has('name')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('nome')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label>Email Address</label>
                                <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required autofocus>
                                <?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label>CPF</label>
                                <input id="cpf" type="text" class="form-control<?php echo e($errors->has('cpf') ? ' is-invalid' : ''); ?>" name="cpf" value="<?php echo e(old('cpf')); ?>" required autofocus>
                                <?php if($errors->has('cpf')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('cpf')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label>Profissão</label>
                                <input id="profissao" type="text" class="form-control<?php echo e($errors->has('profissao') ? ' is-invalid' : ''); ?>" name="profissao" value="<?php echo e(old('profissao')); ?>" required autofocus>
                                <?php if($errors->has('profissao')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('profissao')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-12" style="padding: 0;">
                                <div class="col-md-6  form-group"
                                     style="position: relative; float: left;margin-left:0; ">
                                    <label>Escolaridade</label>
                                    <select name="escolaridade" id="" class="form-control">
                                        <option value=""></option>
                                        <option value="Ensino Fundamental">Ensino Fundamental</option>
                                        <option value="Ensino Medio">Ensino Medio</option>
                                        <option value="Ensino Superior">Ensino Superior</option>
                                        <option value="Mestrado/Doutorado">Mestrado/Doutorado</option>
                                        <option value="Sem Formação">Sem Formação</option>
                                    </select>
                                    <?php if($errors->has('nome')): ?>
                                        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('nome')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-6 form-group"
                                     style="position: relative; float: left;margin-right: 0;">
                                    <label>Faixa Etária</label>
                                    <select name="faixa_etaria" id="" class="form-control">
                                        <option value=""></option>
                                        <option value="Até 50 anos">Até 50 anos</option>
                                        <option value="Entre 21 e 49 anos">Entre 21 e 49 anos</option>
                                        <option value="Até de 20 anos">Até de 20 anos</option>
                                        <option value="Menos de 20 anos">Menos de 20 anos</option>

                                    </select>
                                    <?php if($errors->has('nome')): ?>
                                        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('nome')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Sexo</label>
                                <select name="sexo" id="" class="form-control">
                                    <option value=""></option>
                                    <option value="Feminino">Feminino</option>
                                    <option value="Masculino">Masculino</option>
                                    <option value="Prefiro Não Responder">Prefiro Não Responder</option>

                                </select>
                                <?php if($errors->has('sexo')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('sexo')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label>Rua</label>
                                <input id="rua" type="text"
                                       class="form-control<?php echo e($errors->has('rua') ? ' is-invalid' : ''); ?>" name="rua"
                                       value="<?php echo e(old('rua')); ?>" required autofocus>
                                <?php if($errors->has('rua')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('rua')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-12" style="padding: 0;">
                                <div class="col-md-6 form-group" style="position: relative; float: left;">
                                    <label>CEP</label>
                                    <input id="cep" type="text"
                                           class="form-control<?php echo e($errors->has('cep') ? ' is-invalid' : ''); ?>" name="cep"
                                           value="<?php echo e(old('cep')); ?>" required autofocus>
                                    <?php if($errors->has('cep')): ?>
                                        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('cep')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>

                                <div class="col-md-6 form-group" style="position: relative; float: left;">
                                    <label>Número</label>
                                    <input id="numero" type="text"
                                           class="form-control<?php echo e($errors->has('numero') ? ' is-invalid' : ''); ?>"
                                           name="numero" value="<?php echo e(old('numero')); ?>" required autofocus>
                                    <?php if($errors->has('numero')): ?>
                                        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('numero')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Senha</label>
                                <input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" required>

                                <?php if($errors->has('password')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label>Confirmar Senha</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                            <input type="submit"  class="au-btn au-btn--block au-btn--green m-b-20" value="Cadastrar">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('painel.register', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>