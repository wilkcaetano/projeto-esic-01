<?php $__env->startSection('content'); ?>

        <div class="col-md-12" style="margin-top: 8%;background: white;">

            <table id="example1" class="table table-bordered table-striped">

            <thead>

                <tr>

                <th>Identificação da Solicitação</th>

                <th>Nome Solicitante</th>

                <th>Resposta Usuário</th>

                <th>Resposta Administrador</th>

                <th>Data da Solicitação</th>

                <th>Data Resposta</th>

                <th>Anexo da Solicitação</th>

                <th>Anexo da Resposta</th>

                </tr>

            </thead>

            <?php $__currentLoopData = $resposta; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $respostas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <?php if($respostas->id_solicitacao == $solicitacao->id): ?>

                <tbody>

                    <tr>

                    <td>

                      <?php echo e($solicitacao->id); ?>


                    </td>

                    <td>

                        <?php echo e($user->name); ?>


                    </td>

                    <td>

                        <?php echo e($solicitacao->mensagem); ?>


                    </td>

                    <td>

                        <?php echo e($respostas->resposta); ?>


                    </td>

                    <td>

                        <?php echo e(date('d/m/Y', strtotime($solicitacao->created_at))); ?>


                    </td>

                    <td>

                        <?php echo e(date('d/m/Y', strtotime($respostas->created_at))); ?>


                    </td>

                    <td>

                        <?php if(empty($solicitacao->arquivo)): ?>

                            <?php echo e("Não Consta"); ?>


                        <?php else: ?>

                            <?php echo e($solicitacao->arquivo); ?>


                        <?php endif; ?>

                    </td>

                    <td>

                        <?php if(empty($respostas->arquivo_resposta)): ?>

                            <?php echo e("Não Consta"); ?>


                        <?php else: ?>
                            <a href="<?php echo e(url('public/uploads/arquivos/respostas/'.$respostas->id_users.'/'.$respostas->arquivo_resposta)); ?>" target="_blank">
                                <?php echo e($respostas->arquivo_resposta); ?>

                            </a>
                        <?php endif; ?>

                    </td>

                    </tr>

                </tbody>

                <?php endif; ?>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </table>

        </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>