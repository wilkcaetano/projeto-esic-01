

<?php $__env->startSection('content'); ?>
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">solicitações realizadas</h3>
        <div class="table-data__tool">
            <div class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead>
                    <tr>
                        <th>prioridade</th>
                        <th>tipo</th>
                        <th>mensagem</th>
                        <th>data de criação</th>
                        <th>status</th>
                        <th>Tramitação</th>
                    </tr>
                    </thead>
                    <?php $__currentLoopData = $solicitacao->sortByDesc('id'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $solicitacoes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(Auth::user()->id == $solicitacoes->id_users): ?>
                            <tbody>
                            <tr class="tr-shadow">
                                <td>
                                    <?php echo e($solicitacoes->prioridade); ?>
                                </td>
                                <td>
                                    <?php echo e($solicitacoes->tipo); ?>
                                </td>
                                <td class="desc">
                                    <?php echo e($solicitacoes->mensagem); ?>
                                </td>
                                <td>
                                    <?php echo e(date('d/m/Y H:i:s', strtotime($solicitacoes->created_at))); ?>
                                </td>
                                <td>

                                    <?php if($solicitacoes->status == 'AR'): ?>
                                        <spam style="color: #00a2e3;">
                                            <?php echo e("Foi Aberta"); ?>
                                        </spam>
                                    <?php elseif($solicitacoes->status == 'PR'): ?>
                                        <spam class="status--denied">
                                            <?php echo e("Foi Prorrogado"); ?>
                                        </spam>
                                    <?php elseif($solicitacoes->status == 'RP'): ?>
                                        <spam class="status--process">
                                            <?php echo e("Foi Respondido"); ?>
                                        </spam>
                                    <?php elseif($solicitacoes->status == 'NR'): ?>
                                        <spam class="status--denied">
                                            <?php echo e("Não Respondido"); ?>
                                        </spam>
                                    <?php endif; ?>

                                </td>
                                <td>
                                    <div class="table-data-feature">
                                        <?php if($solicitacoes->status != 'NR'): ?>
                                        <a href="<?php echo e(route('user_tramitacao', $solicitacoes->id)); ?>" class="item" data-toggle="modal">
                                            <i class="zmdi zmdi-more"></i>
                                        </a>
                                        <?php endif; ?>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </table>
            </div>
            <!-- END DATA TABLE -->
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('painel.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>