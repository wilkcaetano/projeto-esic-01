
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Painel de Controle</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo e(asset('painel/css/font-face.css')); ?>" rel="stylesheet" media="all">
    <link href="<?php echo e(asset('painel/vendor/font-awesome-4.7/css/font-awesome.min.css')); ?>" rel="stylesheet" media="all">
    <link href="<?php echo e(asset('painel/vendor/font-awesome-5/css/fontawesome-all.min.css')); ?>" rel="stylesheet" media="all">
    <link href="<?php echo e(asset('painel/vendor/mdi-font/css/material-design-iconic-font.min.css')); ?>" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo e(asset('painel/vendor/bootstrap-4.1/bootstrap.min.css')); ?>" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo e(asset('painel/vendor/animsition/animsition.min.css')); ?>" rel="stylesheet" media="all">
    <link href="<?php echo e(asset('painel/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')); ?>" rel="stylesheet" media="all">
    <link href="<?php echo e(asset('painel/vendor/wow/animate.css')); ?>" rel="stylesheet" media="all">
    <link href="<?php echo e(asset('painel/vendor/css-hamburgers/hamburgers.min.css')); ?>" rel="stylesheet" media="all">
    <link href="<?php echo e(asset('painel/vendor/slick/slick.css')); ?>" rel="stylesheet" media="all">
    <link href="<?php echo e(asset('painel/vendor/select2/select2.min.css')); ?>" rel="stylesheet" media="all">
    <link href="<?php echo e(asset('painel/vendor/perfect-scrollbar/perfect-scrollbar.css')); ?>" rel="stylesheet" media="all">
    <link href="<?php echo e(asset('painel/vendor/vector-map/jqvmap.min.css')); ?>" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo e(asset('painel/css/theme.css')); ?>" rel="stylesheet" media="all">

</head>

<body class="animsition">

<div class="page-wrapper">
    <!-- MENU SIDEBAR-->
    <aside class="menu-sidebar2">
        <div class="logo">
            <a href="#">
                <img src="<?php echo e(asset('img/esic_livre.png')); ?>" alt="Logomarca" style="width: 80%;"/>
            </a>
        </div>
        <div class="menu-sidebar2__content js-scrollbar1">
            <nav class="navbar-sidebar2">
                <ul class="list-unstyled navbar__list">
                    <li class="active has-sub">
                        <a class="js-arrow" href="<?php echo e(route('home')); ?>">
                            <i class="fas fa-home"></i>Home
                            <span class="arrow">
                                    <i class="fas fa-angle"></i>
                                </span>
                        </a>
                    </li>
                    <li class="active has-sub">
                        <a class="js-arrow" href="<?php echo e(route('recentes')); ?>">
                            <i class="fa fa-clone"></i>Solicitações Recentes
                            <span class="arrow">
                                    <i class="fas fa-angle"></i>
                                </span>
                        </a>
                    </li>
                    <li class="active has-sub">
                        <a class="js-arrow" href="<?php echo e(route('abertas')); ?>">
                            <i class="fa fa-clone"></i>Abertas
                            <span class="arrow">
                                    <i class="fas fa-angle"></i>
                                </span>
                        </a>
                    </li>
                    <li class="active has-sub">
                        <a class="js-arrow" href="<?php echo e(route('respondidas')); ?>">
                            <i class="fa fa-clone"></i>Respondidas
                            <span class="arrow">
                                    <i class="fas fa-angle"></i>
                                </span>
                        </a>
                    </li>
                    <li class="active has-sub">
                        <a class="js-arrow" href="<?php echo e(route('prorrogadas')); ?>">
                            <i class="fa fa-clone"></i>Prorrogadas
                            <span class="arrow">
                                    <i class="fas fa-angle"></i>
                                </span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </aside>
    <header class="header-desktop2">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="header-wrap2">
                    <div class="logo d-block d-lg-none">
                        <a href="#">
                            <img src="images/icon/logo-white.png" alt="CoolAdmin" />
                        </a>
                    </div>
                    <div class="header-button2">
                        <div class="header-button-item js-item-menu">
                            <i class="zmdi zmdi-search"></i>
                            <div class="search-dropdown js-dropdown">
                                <form action="">
                                    <input class="au-input au-input--full au-input--h65" type="text" placeholder="Search for datas &amp; reports..." />
                                    <span class="search-dropdown__icon">
                                                <i class="zmdi zmdi-search"></i>
                                            </span>
                                </form>
                            </div>
                        </div>
                        <div class="header-button-item has-noti js-item-menu">
                            <i class="zmdi zmdi-notifications"></i>
                            <div class="notifi-dropdown js-dropdown">
                                <div class="notifi__title">
                                    <p>You have 3 Notifications</p>
                                </div>
                                <div class="notifi__item">
                                    <div class="bg-c1 img-cir img-40">
                                        <i class="zmdi zmdi-email-open"></i>
                                    </div>
                                    <div class="content">
                                        <p>You got a email notification</p>
                                        <span class="date">April 12, 2018 06:50</span>
                                    </div>
                                </div>
                                <div class="notifi__item">
                                    <div class="bg-c2 img-cir img-40">
                                        <i class="zmdi zmdi-account-box"></i>
                                    </div>
                                    <div class="content">
                                        <p>Your account has been blocked</p>
                                        <span class="date">April 12, 2018 06:50</span>
                                    </div>
                                </div>
                                <div class="notifi__item">
                                    <div class="bg-c3 img-cir img-40">
                                        <i class="zmdi zmdi-file-text"></i>
                                    </div>
                                    <div class="content">
                                        <p>You got a new file</p>
                                        <span class="date">April 12, 2018 06:50</span>
                                    </div>
                                </div>
                                <div class="notifi__footer">
                                    <a href="#">All notifications</a>
                                </div>
                            </div>
                        </div>
                        <div class="header-button-item mr-0 js-sidebar-btn">
                            <i class="zmdi zmdi-menu"></i>
                        </div>
                        <div class="setting-menu js-right-sidebar d-none d-lg-block">
                            <div class="account-dropdown__body">
                                <div class="account-dropdown__item">
                                    <?php if(Auth::check()): ?>
                                        <a href="#">
                                            <i class="zmdi zmdi-account"></i><?php echo e(Auth::user()->name); ?>

                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="account-dropdown__body">
                                <div class="account-dropdown__item">
                                    <a href="<?php echo e(route('logout-adm')); ?>" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="zmdi zmdi-power"></i>Logout
                                    </a>
                                    <form id="logout-form" action="<?php echo e(route('logout-adm')); ?>" method="post" style="display: none;">
                                        <?php echo csrf_field(); ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- END MENU SIDEBAR-->
    <div class="page-container2">
        <div class="col-md-12" style="padding: 0px;">
            <?php echo $__env->yieldContent('content'); ?>
        </div>
    </div>
</div>

<!-- Jquery JS-->
<script src="<?php echo e(asset('painel/vendor/jquery-3.2.1.min.js')); ?>"></script>
<!-- Bootstrap JS-->
<script src="<?php echo e(asset('painel/vendor/bootstrap-4.1/popper.min.js')); ?>"></script>
<script src="<?php echo e(asset('painel/vendor/bootstrap-4.1/bootstrap.min.js')); ?>"></script>
<!-- Vendor JS       -->
<script src="<?php echo e(asset('painel/vendor/slick/slick.min.js')); ?>">
</script>
<script src="<?php echo e(asset('painel/vendor/wow/wow.min.js')); ?>"></script>
<script src="<?php echo e(asset('painel/vendor/animsition/animsition.min.js')); ?>"></script>
<script src="<?php echo e(asset('painel/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')); ?>">
</script>
<script src="<?php echo e(asset('painel/vendor/counter-up/jquery.waypoints.min.js')); ?>"></script>
<script src="<?php echo e(asset('painel/vendor/counter-up/jquery.counterup.min.js')); ?>">
</script>
<script src="<?php echo e(asset('painel/vendor/circle-progress/circle-progress.min.js')); ?>"></script>
<script src="<?php echo e(asset('painel/vendor/perfect-scrollbar/perfect-scrollbar.js')); ?>"></script>
<script src="<?php echo e(asset('painel/vendor/chartjs/Chart.bundle.min.js')); ?>"></script>
<script src="<?php echo e(asset('painel/vendor/select2/select2.min.js')); ?>">
</script>
<script src="<?php echo e(asset('painel/vendor/vector-map/jquery.vmap.js')); ?>"></script>
<script src="<?php echo e(asset('painel/vendor/vector-map/jquery.vmap.min.js')); ?>"></script>
<script src="<?php echo e(asset('painel/vendor/vector-map/jquery.vmap.sampledata.js')); ?>"></script>
<script src="<?php echo e(asset('painel/vendor/vector-map/jquery.vmap.world.js')); ?>"></script>

<!-- Main JS-->
<script src="<?php echo e(asset('painel/js/main.js')); ?>"></script>

</body>

</html>
<!-- end document-->