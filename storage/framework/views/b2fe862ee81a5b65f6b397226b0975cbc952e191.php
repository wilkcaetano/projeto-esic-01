<?php

    if(!isset($_GET['ano'])){
        $data = date('Y');
    }else{
        $data = $_GET['ano'];
    }
    $prioridade = \Illuminate\Support\Facades\DB::select("select * from solicitacaos where prioridade = 'Alta'  and year(created_at) = $data");
    $prioridadebaixa = \Illuminate\Support\Facades\DB::select("select * from solicitacaos where prioridade = 'Baixa' and year(created_at) = $data");
    $prioridademedia = \Illuminate\Support\Facades\DB::select("select * from solicitacaos where prioridade = 'Media' and year(created_at) = $data");

/*--------------------------Tipos de Solicitação ----------*/
$pedido_info =\Illuminate\Support\Facades\DB::select("select * from solicitacaos where tipo = 'Pedido de Informação' and year(created_at) = $data");
$rec_info =\Illuminate\Support\Facades\DB::select("select * from solicitacaos where tipo = 'Reclamações' and year(created_at) = $data");
$denu_info =\Illuminate\Support\Facades\DB::select("select * from solicitacaos where tipo = 'Denuncias' and year(created_at) = $data");
$sujest_info =\Illuminate\Support\Facades\DB::select("select * from solicitacaos where tipo = 'Sujestões' and year(created_at) = $data");
$elog_info =\Illuminate\Support\Facades\DB::select("select * from solicitacaos where tipo = 'Elogio' and year(created_at) = $data");

/*--------------------------Faixa Etária ----------*/
$f1 =\Illuminate\Support\Facades\DB::select("select * from users where faixa_etaria = 'Acima de 59 anos' and year(created_at) = $data");
$f2 =\Illuminate\Support\Facades\DB::select("select * from users where faixa_etaria = 'Até 20 anos' and year(created_at) = $data");
$f3 =\Illuminate\Support\Facades\DB::select("select * from users where faixa_etaria = 'De 21 a 40 anos' and year(created_at) = $data");
$f4 =\Illuminate\Support\Facades\DB::select("select * from users where faixa_etaria = 'De 41 a 59 anos' and year(created_at) = $data");

/*-------------- Escolaridade -----------------------*/
$e1 = \Illuminate\Support\Facades\DB::select("select * from users where escolaridade = 'Ensino Fundamental' and year(created_at) = $data");
$e2 = \Illuminate\Support\Facades\DB::select("select * from users where escolaridade = 'Ensino Medio' and year(created_at) = $data");
$e3 = \Illuminate\Support\Facades\DB::select("select * from users where escolaridade = 'Ensino Superior' and year(created_at) = $data");
$e4 = \Illuminate\Support\Facades\DB::select("select * from users where escolaridade = 'Pós-Graduação' and year(created_at) = $data");
$e5 = \Illuminate\Support\Facades\DB::select("select * from users where escolaridade = 'Mestrado/Doutorado' and year(created_at) = $data");
$e6 = \Illuminate\Support\Facades\DB::select("select * from users where escolaridade = 'Sem Instrução Formal' and year(created_at) = $data");

?>

<style>
    #ano:hover{
        background: #edecec;
        transition: 0.5s;
    }
    #ano{
        transition: 0.5s;
    }
    #ano a:link{
        text-decoration: none;
        color: black;
        font-weight: 900;
    }
</style>
<?php $__env->startSection('content'); ?>
    <div class="container" style="min-height: 500px;margin-top:20px;overflow: hidden;margin-bottom: 10px;">
        <div class="col-md-12" id="titulo-graficos" style="text-align: center;height: 50px; background: #edecec;line-height: 50px;font-size: 25px;">
            Gráficos e Estatísticas
        </div>
        <div class="col-md-12" id="menu-ano" style="height: 50px;border: 1px solid #edecec;padding: 0;">
            <?php
            $date = date('Y');
                $select = \Illuminate\Support\Facades\DB::select("SELECT * FROM solicitacaos where year (created_at) = $date");
                foreach ($select as $ano){
                    $yearatual = date('Y', strtotime($ano->created_at));
                }
            $sql = \Illuminate\Support\Facades\DB::select(" select * from solicitacaos where year(created_at) != $date");
            foreach ($sql as $sqls) {
                $yerdiferente = date('Y', strtotime($sqls->created_at));
            }
            ?>
            <?php if(!empty($yerdiferente)): ?>
                <a href="<?php echo e(url('graficos')); ?>?ano=<?php echo e($yerdiferente); ?>">
                    <div class="col-md-3" id="ano" style="border-right: 1px solid #edecec;position: relative;float: left;height: 100%;text-align: center;line-height: 50px;">
                        <?php echo e($yerdiferente); ?>

                    </div>
                </a>
            <?php endif; ?>
                <?php if(!empty($yearatual)): ?>
                    <a href="<?php echo e(url('graficos')); ?>?ano=<?php echo e($yearatual); ?>">
                        <div class="col-md-3" id="ano" style="border-right: 1px solid #edecec;position: relative;float: left;height: 100%;text-align: center;line-height: 50px;">
                            <?php echo e($yearatual); ?>

                        </div>
                    </a>
                <?php endif; ?>
        </div>
        <div class="col-md-12" id="container-graficos" style="margin-top: 10px;border: 1px solid #edecec;min-height: 400px;overflow: hidden;">
            <!--<div class="gif" style="width: 100%; height: 100%;position: relative;float:left;margin-top: 8%;overflow: hidden;background: white;">
                <img src="<?php echo e(asset('img/200.gif')); ?>" alt="" style="margin-left:40%;">
            </div>-->
            <div class="col-md-12" id="container-realgraficos" style="overflow: hidden;">
                <h4 style="text-align: center;">Histórico de Solicitações ano: <?php echo e($data); ?></h4><br>
                <div class="col-md-12" style="background: white;">
                    <canvas id="myChart"></canvas>
                </div>
                <div class="col-md-12">
                    <table class="col-md-12" id="tabela" style="height: 100px; background: #edecec; margin-top: 20px;" width="880">
                        <tbody>

                        <tr>
                            <td style="text-align: center;"><strong>STATUS / MESES</strong></td>
                            <td style="text-align: center;"><strong>JAN</strong></td>
                            <td style="text-align: center;"><strong>FEV</strong></td>
                            <td style="text-align: center;"><strong>MAR</strong></td>
                            <td style="text-align: center;"><strong>ABR</strong></td>
                            <td style="text-align: center;"><strong>MAI</strong></td>
                            <td style="text-align: center;"><strong>JUN</strong></td>
                            <td style="text-align: center;"><strong>JUL</strong></td>
                            <td style="text-align: center;"><strong>AGO</strong></td>
                            <td style="text-align: center;"><strong>SET</strong></td>
                            <td style="text-align: center;"><strong>OUT</strong></td>
                            <td style="text-align: center;"><strong>NOV</strong></td>
                            <td style="text-align: center;"><strong>DEZ</strong></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;"><strong>REALIZADOS</strong></td>
                            <?php
                            for ($i = 1; $i <= 12; $i++) {
                            $sql = \Illuminate\Support\Facades\DB::select("select * from solicitacaos where month(created_at) = $i and year(created_at) = $data");
                            $contador =  count($sql);
                            ?>
                            <td style="text-align: center;"><strong><?php echo e($contador); ?></strong></td>
                            <?php
                            }
                            ?>

                        </tr>
                        <tr>
                            <td style="text-align: center;"><strong>EM ANDAMENTO</strong></td>
                            <?php
                            for ($i = 1; $i <= 12; $i++) {
                            $sql = \Illuminate\Support\Facades\DB::select("select * from solicitacaos where month(created_at) = $i and status = 'PR' and year(created_at) = $data");
                            $contador =  count($sql);
                            ?>
                            <td style="text-align: center;"><strong><?php echo e($contador); ?></strong></td>
                            <?php
                            }
                            ?>
                        </tr>
                        <tr>
                            <td style="text-align: center;"><strong>ATENDIDOS</strong></td>
                            <?php
                            for ($i = 1; $i <= 12; $i++) {
                            $sql = \Illuminate\Support\Facades\DB::select("select * from solicitacaos where month(created_at) = $i and status = 'RP' and year(created_at) = $data");
                            $contador =  count($sql);
                            ?>
                            <td style="text-align: center;"><strong><?php echo e($contador); ?></strong></td>
                            <?php
                            }
                            ?>

                        </tr>
                        </tbody>
                    </table>
                    <hr>
                </div> 
                <div class="col-md-6" style="position: relative; float: left;">
                    <div id="piechart_3d" style="width: 100%; height: 500px;margin-top:20px;"></div>
                </div>
                <div class="col-md-6" style="position: relative; float: left;">
                    <div id="piechart" style="width: 100%; height: 500px;margin-top:20px;"></div>
                </div>
                <div class="col-md-6" style="position: relative; float: left;">
                    <div id="donutchart" style="width: 100%; height: 500px;"></div>
                </div>
                <div class="col-md-6" style="position: relative; float: left;">
                    <div id="piechart1" style="width: 100%; height: 500px;"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        $(function(){
            $("#container-graficos").hide()
            $("#container-realgraficos").css("display","none");
            $("#ano").click(function(){
                $("#container-graficos").fadeIn('slow');
                $(".gif").fadeOut(3000);
                $("#container-realgraficos").delay(3000).fadeIn();
            });
        });
    </script>
    <script src="<?php echo e(asset('js/Chart.bundle.js')); ?>"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Language', 'Speakers (in millions)'],
                ['Alta', <?php echo e(count($prioridade)); ?>], ['Media', <?php echo e(count($prioridademedia)); ?>], ['Baixa', <?php echo e(count($prioridadebaixa)); ?>],

            ]);

            var options = {
                title: 'Prioridade das Solicitações',
                legend: 'none',
                pieSliceText: 'label',
                slices: {  4: {offset: 0.2},
                    12: {offset: 0.3},
                    14: {offset: 0.4},
                    15: {offset: 0.5},
                },
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }
    </script>
    <script>
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['', ''],
                ['Elogio', <?php echo e(count($elog_info)); ?>],
                ['Sujestões',<?php echo e(count($sujest_info)); ?>],
                ['Denuncias', <?php echo e(count($denu_info)); ?>],
                ['Reclamações', <?php echo e(count($rec_info)); ?>],
                ['Pedido de Informação', <?php echo e(count($pedido_info)); ?>]
            ]);

            var options = {
                title: 'Tipos de Solicitação',
                is3D: true,
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
            chart.draw(data, options);
        }
    </script>
    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'bar',

            // The data for our dataset
            data: {
                labels: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto" , "Setembro", "Outubro", "Novembro", "Desembro"],
                datasets: [{
                    label: "Solicitaões Realizadas no ano de <?php echo e($data); ?>",
                    backgroundColor: 'rgb(0, 117, 113)',
                    borderColor: 'rgb(0, 117, 113)',
                    data: [<?php
                        for ($i = 1; $i <= 12; $i++) {
                            $sql = \Illuminate\Support\Facades\DB::select("select * from solicitacaos where month(created_at) = $i and year(created_at) = $data");
                            $contador =  count($sql);
                            echo $contador.',';
                        }
                        ?>],

                },{
                    label: "Solicitaões Respondidas no ano de <?php echo e($data); ?>",
                    backgroundColor: 'rgb(74, 0, 0)',
                    borderColor: 'rgb(74, 0, 0)',
                    data: [<?php
                        for ($i = 1; $i <= 12; $i++) {
                            $sql = \Illuminate\Support\Facades\DB::select("select * from solicitacaos where month(created_at) = $i and status = 'RP'  and year(created_at) = $data");
                            $contador =  count($sql);
                            echo $contador.',';
                        }
                        ?>],
                }]
            },

            // Configuration options go here
            options: {}
        });

    </script>

    <!--Grafico Faixa Etária-->
    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['Acima de 49 anos',     <?php echo e(count($f1)); ?>],
                ['Até 20 anos',      <?php echo e(count($f2)); ?>],
                ['De 21 a 40 anos',  <?php echo e(count($f3)); ?>],
                ['De 41 a 59 anos', <?php echo e(count($f4)); ?>],
            ]);

            var options = {
                title: 'Faixa Etária',
                pieHole: 0.4,
            };

            var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
            chart.draw(data, options);
        }
    </script>
    <!--Grafico Ensino-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Language', 'Speakers (in millions)'],
                ['Ensino Fundamental',  <?php echo e(count($e1)); ?>],
                ['Ensino Medio',  <?php echo e(count($e2)); ?>],
                ['Ensino Superior', <?php echo e(count($e3)); ?>],
                ['Pós-Graduação', <?php echo e(count($e4)); ?>],
                ['Mestrado/Doutorado', <?php echo e(count($e5)); ?>],
                ['Sem Instrução Formal', <?php echo e(count($e6)); ?>],
            ]);

            var options = {
                legend: 'none',
                pieSliceText: 'label',
                title: 'Grau de Escolaridade',
                pieStartAngle: 100,
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart1'));
            chart.draw(data, options);
        }
    </script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frente', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>