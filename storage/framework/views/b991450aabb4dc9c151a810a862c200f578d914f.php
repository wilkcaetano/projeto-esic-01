<?php $__env->startSection('content'); ?>
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">solicitações realizadas</h3>
        <div class="table-data__tool">
            <div class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead>
                    <tr>
                        <th>
                        </th>
                        <th>prioridade</th>
                        <th>tipo</th>
                        <th>mensagem</th>
                        <th>data de criação</th>
                        <th>status</th>
                        <th>resposta</th>
                        <th></th>
                    </tr>
                    </thead>
                    <?php $__currentLoopData = $solicitacao; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $solicitacoes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(Auth::user()->id == $solicitacoes->id_users): ?>
                            <tbody>
                            <tr class="tr-shadow">
                                <td>
                                </td>
                                <td>
                                    <?php echo e($solicitacoes->prioridade); ?>

                                </td>
                                <td>
                                    <?php echo e($solicitacoes->tipo); ?>

                                </td>
                                <td class="desc">
                                    <?php echo e($solicitacoes->mensagem); ?>

                                </td>
                                <td>
                                    <?php echo e(date('d/m/Y H:i:s', strtotime($solicitacoes->created_at))); ?>

                                </td>
                                <td>
                                    <?php if($solicitacoes->status == 'RP'): ?>
                                        <span class="status--process">
                               <?php echo e("Respondido"); ?>

                            </span>
                                <?php elseif($solicitacoes->status == 'NR'): ?>
                                    <span class="status--denied">
                                     <?php echo e("Não Respondido"); ?>

                                <?php elseif($solicitacoes->status == 'PR'): ?>
                                    <span class="status--denied">
                                    <?php echo e("Prorrogada"); ?>

                            </span>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    Não Consta Resposta
                                </td>
                                <td>
                                    <div class="table-data-feature">
                                        <button class="item" data-toggle="modal" data-target="#meuModal">
                                            <i class="zmdi zmdi-more"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </table>
            </div>
            <!-- END DATA TABLE -->
        </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('painel.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>