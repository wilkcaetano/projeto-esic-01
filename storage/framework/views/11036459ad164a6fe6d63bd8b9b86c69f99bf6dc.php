<?php $__env->startSection('content'); ?>

    <!---------------------- Inicio Modal --------------------------------------->

    <?php if(!empty($usuario)): ?>

    <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">

        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="mediumModalLabel">Enviar Mensagens para <?php echo e($usuario->name); ?></h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                    </button>

                </div>

                <div class="modal-body">

                    <p>

                        Formulário de envido de mensagens Curtas

                    </p>

                    <form action="<?php echo e(route('chatMsn')); ?>" method="post" enctype="multipart/form-data">

                        <?php echo e(csrf_field()); ?>


                        <input type="hidden" name="id_users" value="<?php echo e($usuario->id); ?>">

                        <textarea name="chat" id="" style="border: 1px solid #edecec;width: 100%;height: 150px; border-radius: 5px;" cols="1" rows="1">



                        </textarea>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

                    <button type="submit" class="btn btn-primary">Enviar</button>

                </div>

                </form>

            </div>

        </div>

    </div>

    <?php endif; ?>

    <div class="container" style="height: 100%;">

        <div class="col-md-12">

            <h3 class="title-5 m-b-35">lista de usuários cadastrados no sistema</h3>

            <div class="table-responsive table-responsive-data2">

                <table class="table table-data2">

                    <thead>

                    <tr>

                        <th>nome</th>

                        <th>email</th>

                        <th>profissão</th>

                        <th>data</th>

                        <th>sexo</th>

                        <th></th>

                    </tr>

                    </thead>

                    <?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $users): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <tbody>

                        <tr class="tr-shadow">

                            <td><?php echo e($users->name); ?></td>

                            <td>

                                <span class="block-email"><?php echo e($users->email); ?></span>

                            </td>

                            <td class="desc"><?php echo e($users->profissao); ?></td>

                            <td><?php echo e(date('d/m/Y H:m:i', strtotime($users->created_at))); ?></td>

                            <td>

                                <span class="status--process"><?php echo e($users->sexo); ?></span>

                            </td>

                            <td>

                                <div class="table-data-feature">

                                    <a href="<?php echo e(route('chat', $users->id)); ?>" class="item" data-toggle="tooltip" data-placement="top" title="Chat Mensagens Curtas">

                                        <i class="zmdi zmdi-mail-send"></i>

                                    </a>
                                    <a href="<?php echo e(route('delete', $users->id)); ?>" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </a>
                                    <a href="<?php echo e(route('infor-user', $users->id)); ?>" class="item" data-toggle="tooltip" data-placement="top" title="Mais Informações">
                                        <i class="zmdi zmdi-more"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <tr class="spacer"></tr>
                        </tr>
                        </tbody>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </table>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>




<?php echo $__env->make('admin.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>