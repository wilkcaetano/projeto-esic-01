<?php

    $data = date('Y');

    $id_user = Auth::user()->id;

?>

<!DOCTYPE html>

<html lang="pt-br">



<head>

    <!-- Required meta tags-->

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Painel de Controle e-Sic">
    <meta name="author" content="Wilk Caetano">
    <meta name="keywords" content="e-Sic Prodatta">


    <!-- Title Page-->

    <title>Painel de Usuário</title>



    <!-- Fontfaces CSS-->

    <link href="<?php echo e(asset('painel/css/font-face.css')); ?>}" rel="stylesheet" media="all">

    <link href="<?php echo e(asset('painel/vendor/font-awesome-4.7/css/font-awesome.min.css')); ?>}" rel="stylesheet" media="all">

    <link href="<?php echo e(asset('painel/vendor/font-awesome-5/css/fontawesome-all.min.css')); ?>" rel="stylesheet" media="all">

    <link href="<?php echo e(asset('painel/vendor/mdi-font/css/material-design-iconic-font.min.css')); ?>" rel="stylesheet" media="all">



    <!-- Bootstrap CSS-->

    <link href="<?php echo e(asset('painel/vendor/bootstrap-4.1/bootstrap.min.css')); ?>" rel="stylesheet" media="all">



    <!-- Vendor CSS-->

    <link href="<?php echo e(asset('painel/vendor/animsition/animsition.min.css')); ?>" rel="stylesheet" media="all">

    <link href="<?php echo e(asset('painel/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')); ?>" rel="stylesheet" media="all">

    <link href="<?php echo e(asset('painel/vendor/wow/animate.css')); ?>" rel="stylesheet" media="all">

    <link href="<?php echo e(asset('painel/vendor/css-hamburgers/hamburgers.min.css')); ?>" rel="stylesheet" media="all">

    <link href="<?php echo e(asset('painel/vendor/slick/slick.css')); ?>" rel="stylesheet" media="all">

    <link href="<?php echo e(asset('painel/vendor/select2/select2.min.css')); ?>" rel="stylesheet" media="all">

    <link href="<?php echo e(asset('painel/vendor/perfect-scrollbar/perfect-scrollbar.css')); ?>" rel="stylesheet" media="all">



    <!-- Main CSS-->

    <link href="<?php echo e(asset('painel/css/theme.css')); ?>" rel="stylesheet" media="all">



</head>



<body class="animsition">

<!-- modal large -->

<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content">

            <div class="modal-header">

                <h5 class="modal-title" id="largeModalLabel">Solicitação</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                    <span aria-hidden="true">&times;</span>

                </button>

            </div>

            <div class="modal-body">

                <?php if(Session::has('message')): ?>

                    <div id="msg" class="alert alert-success" style="text-align: center;">

                        <p>

                            <?php echo e(Session::get('message')); ?>




                        </p>

                    </div>

                <?php endif; ?>  

            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>

            </div>

        </div>

    </div>

</div>

<!-- end modal large -->



<div class="page-wrapper">

    <!-- HEADER MOBILE-->

    <header class="header-mobile d-block d-lg-none">

        <div class="header-mobile__bar">

            <div class="container-fluid">

                <div class="header-mobile-inner">

                    <a class="logo" href="index.html">

                        <img src="<?php echo e(asset('img/esic_livre.png')); ?>" alt="CoolAdmin" />

                    </a>

                    <button class="hamburger hamburger--slider" type="button">

                            <span class="hamburger-box">

                                <span class="hamburger-inner"></span>

                            </span>

                    </button>

                </div>

            </div>

        </div>

        <nav class="navbar-mobile">

            <div class="container-fluid">

                <ul class="navbar-mobile__list list-unstyled">

                    <ul class="list-unstyled navbar__list">

                        <li class="active has-sub">

                            <a class="js-arrow" href="<?php echo e(route('home')); ?>">

                                <i class="fas fa-home"></i>Home

                            </a>

                        </li>

                        <li class="active has-sub">

                            <a class="js-arrow" href="<?php echo e(route('solicitar')); ?>">

                                <i class="fas fa-paper-plane"></i>Solicitar Informação

                            </a>

                        </li>

                        <li class="active has-sub">

                            <a class="js-arrow" href="<?php echo e(route('acompanhar')); ?>">

                                <i class="fas fa-clone"></i>Acompanhar Solicitação

                            </a>

                        </li>

                    </ul>

                </ul>

            </div>

        </nav>

    </header>

    <!-- END HEADER MOBILE-->



    <!-- MENU SIDEBAR-->

    <aside class="menu-sidebar d-none d-lg-block">

        <div class="logo">

            <a href="#">

                <img src="<?php echo e(asset('img/logo_site_2.png')); ?>" alt="CoolAdmin" style="width: 80%;"/>

            </a>

        </div>

        <div class="menu-sidebar__content js-scrollbar1">

            <nav class="navbar-sidebar">

                <ul class="list-unstyled navbar__list">

                    <li class="active has-sub">

                        <a class="js-arrow" href="<?php echo e(route('home')); ?>">

                            <i class="fas fa-home"></i>Home

                        </a>

                    </li>

                    <li class="active has-sub">

                        <a class="js-arrow" href="<?php echo e(route('solicitar')); ?>">

                            <i class="fas fa-paper-plane"></i>Solicitar Informação

                        </a>

                    </li>

                    <li class="active has-sub">

                        <a class="js-arrow" href="<?php echo e(route('acompanhar')); ?>">

                            <i class="fas fa-clone"></i>Acompanhar Solicitação

                        </a>

                    </li>

                </ul>

            </nav>

        </div>

    </aside>

    <!-- END MENU SIDEBAR-->



    <!-- PAGE CONTAINER-->

    <div class="page-container">

        <!-- HEADER DESKTOP-->

        <header class="header-desktop">

            <div class="section__content section__content--p30">

                <div class="container-fluid">

                    <div class="header-wrap">

                        <form class="form-header" action="" method="POST">

                            <!--<input class="au-input au-input--xl" type="text" name="search" placeholder="Search for datas &amp; reports..." />

                            <button class="au-btn--submit" type="submit">

                                <i class="zmdi zmdi-search"></i>

                            </button>-->

                        </form>

                        <div class="header-button">

                            <div class="noti-wrap">

                                <div class="noti__item js-item-menu">

                                    <i class="zmdi zmdi-email"></i>

                                    <span class="quantity">

                                        <?php

                                            $total = \Illuminate\Support\Facades\DB::select("select * from chat where status = 'Fechada' and id_users = ?", [$id_user]);

                                            echo count($total);

                                        ?>

                                    </span>

                                    <div class="email-dropdown js-dropdown">



                                        <div class="email__title">



                                            <p>

                                                <?php

                                                $total = \Illuminate\Support\Facades\DB::select("select * from chat where status = 'Fechada' and id_users = ?", [$id_user]);

                                                echo "Você possui ".count($total)." mensagens";

                                                ?>

                                            </p>

                                        </div>

                                        <?php

                                        $chat = \Illuminate\Support\Facades\DB::select("select * from chat where status = 'Fechada' and id_users = ?", [$id_user]);

                                        foreach ($chat as $chats){

                                        ?>

                                        <div class="email__item">

                                            <div class="content">

                                                <p>
                                                    <a href="<?php echo e(route('resposta_chat', $chats->id)); ?>">
                                                        <?php echo e($chats->chat); ?>

                                                    </a>
                                                </p>

                                                <span></span>

                                            </div>

                                        </div>

                                        <?php

                                        }

                                        ?>



                                    </div>

                                </div>

                                <div class="noti__item js-item-menu">

                                    <i class="zmdi zmdi-notifications"></i>

                                    <span class="quantity">3</span>

                                    <div class="notifi-dropdown js-dropdown">

                                        <div class="notifi__title">

                                            <p>You have 3 Notifications</p>

                                        </div>

                                        <div class="notifi__item">

                                            <div class="bg-c1 img-cir img-40">

                                                <i class="zmdi zmdi-email-open"></i>

                                            </div>

                                            <div class="content">

                                                <p>You got a email notification</p>

                                                <span class="date">April 12, 2018 06:50</span>

                                            </div>

                                        </div>

                                        <div class="notifi__item">

                                            <div class="bg-c2 img-cir img-40">

                                                <i class="zmdi zmdi-account-box"></i>

                                            </div>

                                            <div class="content">

                                                <p>Your account has been blocked</p>

                                                <span class="date">April 12, 2018 06:50</span>

                                            </div>

                                        </div>

                                        <div class="notifi__item">

                                            <div class="bg-c3 img-cir img-40">

                                                <i class="zmdi zmdi-file-text"></i>

                                            </div>

                                            <div class="content">

                                                <p>You got a new file</p>

                                                <span class="date">April 12, 2018 06:50</span>

                                            </div>

                                        </div>

                                        <div class="notifi__footer">

                                            <a href="#">All notifications</a>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="account-wrap">

                                <div class="account-item clearfix js-item-menu">

                                    <div class="content">

                                        <?php if(Auth::check()): ?>

                                            <a class="js-acc-btn" href="#"><?php echo e(Auth::user()->name); ?></a>

                                        <?php endif; ?>

                                    </div>

                                    <div class="account-dropdown js-dropdown">

                                        <div class="account-dropdown__footer">

                                            <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"

                                               onclick="event.preventDefault();

                                                     document.getElementById('logout-form').submit();">

                                                <i class="zmdi zmdi-power"></i>Logout

                                            </a>



                                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">

                                                <?php echo csrf_field(); ?>

                                            </form>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </header>

        <!-- HEADER DESKTOP-->



        <!-- MAIN CONTENT-->

        <div class="main-content">

            <div class="section__content section__content--p30">

                <div class="container-fluid">

                    <?php echo $__env->yieldContent('content'); ?>

                </div>

            </div>

        </div>

        <!-- END MAIN CONTENT-->

        <!-- END PAGE CONTAINER-->



<!-- Jquery JS-->

<script src="<?php echo e(asset('painel/vendor/jquery-3.2.1.min.js')); ?>"></script>

<!-- Bootstrap JS-->

<script src="<?php echo e(asset('painel/vendor/bootstrap-4.1/popper.min.js')); ?>"></script>

<script src="<?php echo e(asset('painel/vendor/bootstrap-4.1/bootstrap.min.js')); ?>"></script>

<!-- Vendor JS       -->

<script src="<?php echo e(asset('painel/vendor/slick/slick.min.js')); ?>">

</script>

<script src="<?php echo e(asset('painel/vendor/wow/wow.min.js')); ?>"></script>

<script src="<?php echo e(asset('painel/vendor/animsition/animsition.min.js')); ?>"></script>

<script src="<?php echo e(asset('painel/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')); ?>">

</script>

<script src="<?php echo e(asset('painel/vendor/counter-up/jquery.waypoints.min.js')); ?>"></script>

<script src="<?php echo e(asset('painel/vendor/counter-up/jquery.counterup.min.js')); ?>">

</script>

<script src="<?php echo e(asset('painel/vendor/circle-progress/circle-progress.min.js')); ?>"></script>

<script src="<?php echo e(asset('painel/vendor/perfect-scrollbar/perfect-scrollbar.js')); ?>"></script>

<script src="<?php echo e(asset('painel/vendor/chartjs/Chart.bundle.min.js')); ?>"></script>

<script src="<?php echo e(asset('painel/vendor/select2/select2.min.js')); ?>">

</script>



<!-- Main JS-->

<script src="<?php echo e(asset('painel/js/main.js')); ?>"></script>

        <?php if(Session::has('message')): ?>

            <script>

                $(document).ready(function(){

                    $("#largeModal").modal();

                })

            </script>

        <?php endif; ?>



        <script type="text/javascript">

            try {

                //bar chart

                var ctx = document.getElementById("barChart");

                if (ctx) {

                    ctx.height = 200;

                    var myChart = new Chart(ctx, {

                        type: 'bar',

                        defaultFontFamily: 'Poppins',

                        data: {

                            labels: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],

                            datasets: [

                                {

                                    label: "Solicitaçoes Realizadas",

                                    data: [<?php

                                            $id_user = Auth::user()->id;

                                        for ($i = 1; $i <= 12; $i++) {

                                            $sql = \Illuminate\Support\Facades\DB::select("select * from solicitacaos where month(created_at) = $i and id_users = $id_user");

                                            $contador =  count($sql);

                                            echo $contador.',';

                                        }

                                        ?>



                                    ],

                                    borderColor: "rgba(0, 123, 255, 0.9)",

                                    borderWidth: "0",

                                    backgroundColor: "rgba(0, 123, 255, 0.5)",

                                    fontFamily: "Poppins"

                                },

                                {

                                    label: "Solicitações Respondidas",

                                    data: [

                                        <?php

                                        $id_user = Auth::user()->id;

                                        for ($i = 1; $i <= 12; $i++) {

                                            $sql = \Illuminate\Support\Facades\DB::select("select * from solicitacaos where month(created_at) = $i and status = 'RP'  and id_users = $id_user");

                                            $contador =  count($sql);

                                            echo $contador.',';

                                        }

                                        ?>

                                    ],

                                    borderColor: "rgba(0,0,0,0.09)",

                                    borderWidth: "0",

                                    backgroundColor: "rgba(0,0,0,0.07)",

                                    fontFamily: "Poppins"

                                }

                            ]

                        },

                        options: {

                            legend: {

                                position: 'top',

                                labels: {

                                    fontFamily: 'Poppins'

                                }



                            },

                            scales: {

                                xAxes: [{

                                    ticks: {

                                        fontFamily: "Poppins"



                                    }

                                }],

                                yAxes: [{

                                    ticks: {

                                        beginAtZero: true,

                                        fontFamily: "Poppins"

                                    }

                                }]

                            }

                        }

                    });

                }





            } catch (error) {

                console.log(error);

            }



        </script>



</body>





</html>

<!-- end document-->

