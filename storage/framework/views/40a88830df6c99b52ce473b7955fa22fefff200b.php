<?php $__env->startSection('content'); ?>
<style>
    .cor{
        color: #1f6fb2;
    }
</style>
<div class="col-md-12">
    <div class="col-md-12">
        <aside class="profile-nav alt">
            <section class="card">
                <div class="card-header user-header alt bg-dark">
                    <div class="media">
                        <a href="#">
                            <?php if($adm->sexo == "Masculino"): ?>
                                <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="<?php echo e(url('public/img/avatar-homem.jpg')); ?>">
                            <?php elseif($adm->sexo == ""): ?>
                                <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="<?php echo e(url('public/img/avatar-homem.jpg')); ?>">
                            <?php else: ?>
                                <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="<?php echo e(url('public/img/avatar-mulher.jpg')); ?>">
                            <?php endif; ?>
                        </a>
                        <div class="media-body">
                            <h2 class="text-light display-6"><?php echo e($adm->name); ?></h2>
                            <p><?php echo e($adm->cargo); ?></p>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <spam class="cor">ID:</spam> <?php echo e($adm->id); ?>

                    </li>
                    <li class="list-group-item">
                        <spam class="cor">Email: </spam> <?php echo e($adm->email); ?>

                    </li>
                    <li class="list-group-item">
                        <a href="javascript:history.back()" class="btn btn-primary">Voltar</a>
                        <a href="<?php echo e(route('delet-adm', $adm->id)); ?>" class="btn btn-danger">Excluir Usuário</a>
                    </li>
                </ul>

            </section>
        </aside>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>