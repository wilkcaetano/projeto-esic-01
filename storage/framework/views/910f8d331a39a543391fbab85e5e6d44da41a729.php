<!DOCTYPE html>

<html>

<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <meta http-equiv="refresh" content="3; http://localhost:8000"> -->

    <title>e-Sic</title>

    <link rel="stylesheet" href="<?php echo e(asset('principal/assets/bootstrap/css/bootstrap.min.css')); ?>">

    <link rel="stylesheet" href="<?php echo e(asset('principal/assets/fonts/font-awesome.min.css')); ?>">

    <link rel="stylesheet" href="<?php echo e(asset('principal/assets/fonts/ionicons.min.css')); ?>">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Archivo+Black">

    <link rel="stylesheet" href="<?php echo e(asset('principal/assets/css/styles.min.css')); ?>">
    
    <link rel="stylesheet" href="<?php echo e(asset('principal/assets/css/efeito.css')); ?>" media="screen and (max-width: 765px)">

</head>



<body>

<div id="cabecalho" class="row" style="margin:0px;min-height:150px;background-color:#edecec;padding:0;">

    <div id="central-cabecalho" class="container">

        <div id="linha-logo-busca" class="col-md-12" style="height:100px;">

            <div id="espaco" class="d-md-none col-sm-3 col-xs-3" style="height:10px;"></div>

            <div id="lado-logomarca" class="col-md-6 col-sm-6" style="height:100%;"><img src="<?php echo e(asset('principal/assets/img/logo_site_2.png')); ?>" style="position:sticky;top:10px;width: 250px;"></div>

            <div id="lado-busca" class="col-md-6" style="height:100%;">

                <div id="efeito" class="d-none d-md-none col-sm-2"></div>

                <div id="rede-social" class="col-md-12 col-sm-6 col-xs-6" style="font-size:25px;"><a href="<?php echo e(route('login')); ?>"><i class="icon ion-log-in" style="margin-right:15px;position:relative;"></i></a> <a href="<?php echo e(route('register')); ?>"> <i class="fa fa-user-plus" style="margin-right:15px;"></i></a><a href="<?php echo e(url('/admin/login')); ?>"><i class="fa fa-gears"></i></a></div>

            </div>

        </div>

    </div>

    <div id="linha-menu" class="col-md-12" style="min-height:50px;overflow: ;">
        
        
        <div id="menu-central" class="container" >
        
            <div class="navbar navbar-expand-md navbar-dark bg-dark" style="background-color:#edecec!important;">
        
                <div class="container" style="color:#000000;"><span class="text-white d-md-none" style="color:rgb(0,0,0)!important;">e-Sic Câmara de Machados</span><button class="btn btn-link navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav"><i class="fa fa-align-justify" style="color:rgba(0,0,0,0.5);"></i></button>
        
                    <div
        
                            id="main-nav" class="navbar-collapse collapse">
        
                        <ul class="navbar-nav nav-fill w-100">
        
                            <li class="nav-item"><a href="<?php echo e(url('/')); ?>" class="nav-link"  style="color:rgb(0,0,0);">Home</a></li>
        
                            <li class="nav-item"><a href="#como" class="nav-link" id="scrollLink" style="color:rgb(0,0,0);">Como Solicitar Uma Informação</a></li>
        
                            <li class="nav-item"><a href="#solicitacao-presencial" class="nav-link" id="scrollLink" style="color:rgb(0,0,0);">Solicitação Presencial</a></li>
        
                            <a href="<?php echo e(route('graficos')); ?>" class="nav-link" style="color:rgb(0,0,0);"> <li class="nav-item">Gráficos e Estatísticas</li></a>
                            
                            <li class="nav-item" id="efeito">
                                <a href="#" class="nav-link" id="scrollLink"  style="color:rgb(0,0,0);">
                                    Lai
                                </a>
                                <div class="submenu">
                                    <div class="item">Teste</div>
                                    <div class="item">Teste</div>
                                    <div class="item">Teste</div>
                                    <div class="item">Teste</div>
                                </div>
                            </li>
        
                        </ul>
        
                    </div>
        
                </div>
        
            </div>
        
        </div>

    </div>

</div>

    <?php echo $__env->yieldContent('content'); ?>



    <div class="footer-clean" style="background-color:rgb(1,1,1);color:rgb(250,252,255);">

        <footer>

            <div class="container">

                <div class="row justify-content-center">

                    <div class="col-sm-12 col-md-12 item" style="position: relative;float: left;">
                        <div class="col-md-3" style="margin: 0 auto;overflow: hidden;">
                            <img src="<?php echo e(asset('principal/assets/img/logo_site_2.png')); ?>" style="position:sticky;top:10px;width: 250px;">
                        </div>

                    </div>


                    <div class="col-lg-12" style="position: relative;float: left;text-align: center">
                        <p class="copyright">Todos os Direitos Reservados a Câmara Municipal de Machados-PE © <?php echo e(date('Y')); ?> Desenvolvido por
                            <a href="http://prodatta.com">Prodatta</a></p>

                    </div>

                </div>

            </div>

        </footer>

    </div>

    <script src="<?php echo e(asset('principal/assets/js/jquery.min.js')); ?>"></script>

    <script src="<?php echo e(asset('principal/assets/bootstrap/js/bootstrap.min.js')); ?>"></script>

    <script>

        $(document).ready(function(){

            $( "a#scrollLink" ).click(function( event ) {

                event.preventDefault();

                $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 1500);

            });

        });

    </script>

</body>



</html>