<?php $__env->startSection('content'); ?>
<div class="page-wrapper">
    <div class="page-content--bge5">
        <div class="container">
            <div class="login-wrap">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="#">
                            <img src="<?php echo e(asset('public/img/esic_livre.png')); ?>" alt="CoolAdmin">
                        </a>
                    </div>
                    <div class="login-form">
                        <form method="POST" action="<?php echo e(route('password.request')); ?>" aria-label="<?php echo e(__('Reset Password')); ?>">
                            <?php echo csrf_field(); ?>
                            <input type="hidden" name="token" value="<?php echo e($token); ?>">
                            <div class="form-group">
                                <label>Email</label>
                                <input class="au-input au-input--full<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" type="email" name="email" placeholder="Email" value="<?php echo e($email ?? old('email')); ?>" required autofocus>
                                <?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label>Senha</label>
                                <input class="au-input au-input--full<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" type="password" name="password" placeholder="Senha" required>

                                <?php if($errors->has('password')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label>Confirmar Senha</label>
                                <input class="au-input au-input--full" id="password-confirm" type="password" name="password_confirmation" placeholder="Confirmar Senha" required>
                            </div>
                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Redefinir</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('painel.login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>