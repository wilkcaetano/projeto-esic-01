<?php $__env->startSection('content'); ?>



<div id="banner-linha" class="row" style="margin:0px;padding:10px;">
    <div id="banner-central" class="container" style="height:auto;">
        <div class="carousel slide" data-ride="carousel" id="carousel-1" style="height:auto;">
            <div class="carousel-inner" role="listbox" style="height:auto;">
                <div class="carousel-item "><img class="w-100 d-block" src="<?php echo e(asset('principal/assets/img/banner03.jpg')); ?>" alt="Slide Image"></div>
                <div class="carousel-item active"><img class="w-100 d-block" src="<?php echo e(asset('principal/assets/img/banner.jpg')); ?>" alt="Slide Image"></div>
                <div class="carousel-item" style="height:auto;"><img class="w-100 d-block" src="<?php echo e(asset('principal/assets/img/banner03.png')); ?>" alt="Slide Image"></div>
            </div>
            <div><a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev"><span class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#carousel-1" role="button" data-slide="next"><span class="carousel-control-next-icon"></span><span class="sr-only">Next</span></a></div>
            <ol class="carousel-indicators">
                <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-1" data-slide-to="1"></li>
                <li data-target="#carousel-1" data-slide-to="2"></li>
            </ol>
        </div>
    </div>
</div>
<div id="como-solicitar" class="row">
    <div id="solicitar-container" class="container">
        <div id="como"></div>
        <div id="titulo-container" class="col-md-12" style="background-color:#edecec;"><span style="font-family:'Archivo Black', sans-serif;">Como Solicitar Uma Informação</span></div>
        <div id="lado-esquerdo-solicitar" class="col-md-6">
            <div id="titulo-eletronico" class="col-md-12" style="min-height:50px;text-align:center;line-height:50px;overflow: hidden;"><span style="font-family:'Archivo Black', sans-serif;font-size:18px;">Pedido de Solicitação Eletrônico</span></div>
            <p>1. Acesse o e-SIC (Sistema Eletrônico do Serviço de Informações ao Cidadão): &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br></p>
            <p>2. No icone <i class="fa fa-user-plus"></i>&nbsp; você será direcionado a uma pagina de cadastro, existem itens que possivelmente será obrigatório e outros por apenas coletas de dados. &nbsp; &nbsp; &nbsp;&nbsp;<br></p>
            <p>3. Apartir do cadastro você será direcionado para uma pagina de login onde o login será o email que você cadastrou e a senha que foi cadastrada. Lebramos que será enviado um email com senha caso você precise guarda.</p>
            <p>4. No painel você terá a possibilidade de solicitar informação e acompanhar cada informação solicitada, lembrando que cada solicitação será enviado emils para informar a tramitação da sua solicitação.</p>
        </div>
        <div id="lado-direito-solicitar" class="col-md-6" style="height:100%;">
            <div id="titulo-eletronico" class="col-md-12" style="height:50px;text-align:center;line-height:50px;"><span style="font-family:'Archivo Black', sans-serif;font-size:18px;">Passo a passo</span></div>
            <div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/PrlwF_mgROw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
        </div>
    </div>
</div>

<div id="como-solicitar" class="row">
    <div id="solicitar-container" class="container">
        <div id="solicitacao-presencial"></div>
        <div id="titulo-container" class="col-md-12" style="background-color:#edecec;"><span style="font-family:'Archivo Black', sans-serif;">Solicitação Presencial</span></div>
        <div id="lado-esquerdo-solicitar" class="col-md-12">
            <div id="titulo-eletronico" class="col-md-12" style="min-height:50px;text-align:center;line-height:50px;position: relative;float: left;"><span style="font-family:'Archivo Black', sans-serif;font-size:18px;">Pedido de Solicitação Presencial</span></div>
            <p>1. Dirija-se à unidade física do SIC pertencente ao orgão ao qual você pretende solicitar a informação.<br></p>
            <p>2. Preencha o Formulário de Acesso:&nbsp;<br><a href="http://acaotranparencia.prodatta.inf.br/cmgoiana/manual/arquivos/formulario-solicitacao-informacao-pn.pdf">Pessoa natural</a>&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;<a href="http://acaotranparencia.prodatta.inf.br/cmgoiana/manual/arquivos/formulario-solicitacao-informacao-pj.pdf">Pessoa jurídica</a>&nbsp;<br></p>
            <p>3. Aguarde a inserção da solicitação no e-SIC e receba o seu número de protocolo. Guarde o seu número de protocolo, pois ele é o comprovante do cadastro da solicitação via sistema.&nbsp;<br></p>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frente', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>