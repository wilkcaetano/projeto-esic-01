<style>

    #container-pricipal{

        background: white;

        border-radius: 5px;

        margin: 5px;

        overflow: hidden;

    }

    #linha{

        height: 5px;

        background: royalblue;

    }



    #linha-cont{

        height: 30px;

        border-bottom: 1px solid #d4edda;

        background: #edecec;

        text-align: center;

        line-height: 30px;

    }

    #menor{

        position: relative;

        float: left;

    }



    #conteudo-linha{

        padding: 0;

    }

    #conteudo-linhas-menores{

        min-height: 35px;

        line-height: 35px;

        border: 1px solid #edecec;

        text-align: center;

        position: relative;

        float: left;

        font-size: 15px;

        color: blue;

    }

</style>





<?php $__env->startSection('content'); ?>

    <div class="container-fluid" id="container-pricipal" style="position: relative; float: left;">

        <div class="col-md-12" id="conteudo-solicitacoes">

            <h4>Solicitação Realizada por <?php echo e($user->name); ?></h4>

            <hr>

            <input type="hidden" name="id" value="<?php echo e($user['id']); ?>">

            <div class="col-md-12" style="padding: 0;position: relative;float: left;">

                <div class="col-md-12" id="linha-cont">

                    <div class="col-md-3" id="menor">

                        Nome do Solicitante

                    </div>

                    <div class="col-md-3" id="menor">

                        Email do Solicitante

                    </div>

                    <div class="col-md-3" id="menor">

                        Status da Solicitação

                    </div>

                    <div class="col-md-3" id="menor">

                        Prorrogada ?

                    </div>

                </div>

                <div class="col-md-12" id="conteudo-linha">

                    <div class="col-md-3" id="conteudo-linhas-menores">

                        <?php echo e($user->name); ?>


                    </div>

                    <div class="col-md-3" id="conteudo-linhas-menores">

                        <?php echo e($user->email); ?>


                    </div>

                    <div class="col-md-3" id="conteudo-linhas-menores">

                        <?php

                        if($solicitacao['status'] == 'AR'){

                            echo "Aberta";

                        }elseif($solicitacao['status'] == 'RP'){

                            echo "Respondida";

                        }elseif($solicitacao['status'] == 'PR'){

                            echo "Prorrogada";

                        }

                        ?>

                    </div>

                    <div class="col-md-3" id="conteudo-linhas-menores">

                        <?php if($solicitacao->status == 'PR'): ?>

                            <?php echo e('Sim'); ?>


                        <?php else: ?>

                            <?php echo e('Não'); ?>


                        <?php endif; ?>

                    </div>

                </div>

            </div>

            <div class="col-md-12" style="padding: 0;position: relative;float: left;">

                <div class="col-md-12" id="linha-cont">

                    <div class="col-md-6" id="menor">

                        Mensagem

                    </div>

                    <div class="col-md-6" id="menor">

                       Anexo Usuário

                    </div>

                </div>

                <div class="col-md-12" id="conteudo-linha">

                    <div class="col-md-6" id="conteudo-linhas-menores">

                        <?php echo e($solicitacao->mensagem); ?>


                    </div>

                    <div class="col-md-6" id="conteudo-linhas-menores">

                        <?php if($solicitacao->arquivo == ""): ?>

                            <?php echo e("Não Consta Nenhum Anexo Na Solicitação"); ?>


                        <?php else: ?>

                            <a href='<?php echo e(url('public/uploads/arquivos/'.$user->id.'/'.$solicitacao->arquivo)); ?>'>Anexo</a>

                        <?php endif; ?>

                    </div>

                </div>

            </div>

            <div class="col-md-12" style="padding: 0;position: relative;float: left;">

                <div class="col-md-12" id="linha-cont">

                    <div class="col-md-6" id="menor">

                       Respomder Solicitação

                    </div>

                    <div class="col-md-6" id="menor">

                        Data Para Resposta

                    </div>

                </div>

                <div class="col-md-12" id="conteudo-linha">

                    <div class="col-md-6" id="conteudo-linhas-menores">

                        <a href="<?php echo e(route('respondendo', $solicitacao->id)); ?>">

                            <button class="btn btn-primary" style="margin: 10px;">

                                Resposnder

                            </button>

                        </a>

                    </div>

                    <div class="col-md-6" id="conteudo-linhas-menores">

                        <?php if($solicitacao['status'] != "PR"): ?>

                            <?php

                            $id =  $solicitacao['id_solicitante'];

                            $datafinal = new DateTime('+ 20 days'.$solicitacao['created_at']);

                            $dataatual = new DateTime();

                            date_default_timezone_set('America/Sao_Paulo');

                            $total = $dataatual->diff($datafinal);

                            $result = $total->format('%R%a Dias');

                            if($result > 5){

                                $result =  "<b style='color:darkblue'>".$result."</b>";

                                echo "Você tem ".$result." para responder a solicitação de ".$user->name;

                            }else{



                                $result =  "<b style='color:darkred'>".$result."</b>";

                                echo "Passou $result de prazo de resposta para a solicitação";

                            }

                            ?>

                        <?php elseif($solicitacao['status'] == "PR"): ?>

                            <?php

                            $id =  $solicitacao['id_solicitante'];

                            $datafinal = new DateTime ('+ 30 days'.$solicitacao['created_at']);

                            $dataatual = new DateTime();

                            //date_default_timezone_set('America/Sao_Paulo');

                            $total = $dataatual->diff($datafinal);

                            $result = $total->format('%R%a Dias');

                            if($result > 5){

                                $result =  "<b style='color:darkblue'>".$result."</b>";

                            }else{



                                $result =  "<b style='color:darkred'>".$result."</b>";





                            }





                            echo "Você tem ".$result." para responder a solicitação de ".$user->name;

                            ?>

                        <?php endif; ?>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>