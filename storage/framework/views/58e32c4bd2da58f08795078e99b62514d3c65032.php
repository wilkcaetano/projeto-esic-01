<?php $__env->startSection('content'); ?>
<div class="page-wrapper">
    <div class="page-content--bge5">
        <div id="linha-menu" class="col-md-12" style="min-height:50px;overflow: hidden;">
            <div id="menu-central" class="container" >
                <div class="navbar navbar-expand-md navbar-dark bg-dark" style="background-color:rgba(0,0,0, 0.0)!important;border-bottom: 1px solid gainsboro;">
                    <div class="container" style="color:#000000;"><span class="text-white d-md-none" style="color:rgb(0,0,0)!important;">e-Sic Câmara</span><button class="btn btn-link navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav"><i class="fa fa-align-justify" style="color:rgba(0,0,0,0.5);"></i></button>
                        <div
                                id="main-nav" class="navbar-collapse collapse">
                            <ul class="navbar-nav nav-fill w-100">
                                <li class="nav-item col-md-2"><a href="<?php echo e(url('/')); ?>" class="nav-link"  style="color:rgb(0,0,0);">Home</a></li>
                                <a href="<?php echo e(route('graficos')); ?>" class="nav-link col-md-2" style="color:rgb(0,0,0);"> <li class="nav-item">Gráficos e Estatísticas</li></a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="login-wrap">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="#">
                            <img src="<?php echo e(url('img/esic_livre.png')); ?>" alt="CoolAdmin">
                        </a>
                    </div>
                    <div class="login-form">
                        <form method="POST" action="<?php echo e(route('login')); ?>" aria-label="<?php echo e(__('Login')); ?>">
                            <?php echo e(csrf_field()); ?>
                            <div class="form-group">
                                <label>Email</label>
                                <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required autofocus>
                                <?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label>Senha</label>
                                <input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" required>
                                <?php if($errors->has('password')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="login-checkbox">
                                <label>
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>Lembre de mim
                                </label>
                                <label>
                                    <a class="btn btn-link" href="<?php echo e(route('password.request')); ?>">
                                        <?php echo e(__('Esqueceu sua senha?')); ?>
                                    </a>
                                </label>
                            </div>
                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>
                        </form>
                        <div class="register-link">
                            <p>
                                Você não tem conta?
                                <a href="<?php echo e(route('register')); ?>">Assine aqui</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('painel.login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>