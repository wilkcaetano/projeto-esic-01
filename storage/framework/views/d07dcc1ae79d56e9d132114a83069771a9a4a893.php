<?php $__env->startSection('content'); ?>
    <!-- modal large -->
    <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">Resposta da Solicitacão</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Nome Solicitante</th>
                                <th>Resposta</th>
                                <th>Data da Solicitação</th>
                                <th>Data Resposta</th>
                                <th>Anexo da Solicitação</th>
                                <th>Anexo da Resposta</th>
                            </tr>
                            </thead>
                            <?php $__currentLoopData = $solicitacao; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $solicitacoes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($solicitacoes->status == 'RP'): ?>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php echo e($user->name); ?>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </td>
                                        <td>
                                            <?php echo e($solicitacoes->tipo); ?>

                                        </td>
                                        <td>
                                            <?php echo e(date('d/m/Y', strtotime($solicitacoes->created_at))); ?>

                                        </td>
                                        <td>
                                            <?php
                                            $datacri = $solicitacoes->created_at;
                                            echo date('d/m/y', strtotime($datacri.'+ 20 days'));
                                            ?>
                                        </td>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                    </tbody>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal large -->
    <!-- modal large -->
    <div class="box" style="margin-top: 8%;">
        <div class="box-header">
            <h3 class="box-title">Solicitações Que Foram Respondidas!</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Estado</th>
                    <th>Nome Solicitante</th>
                    <th>Tipo</th>
                    <th>Data da Solicitação</th>
                    <th>Data Resposta</th>
                </tr>
                </thead>
                <?php $__currentLoopData = $solicitacao; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $solicitacoes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($user->id == $solicitacoes->id_users): ?>
                    <?php if($solicitacoes->status == 'RP'): ?>
                        <tbody>
                        <tr>
                            <td>
                                <a href="#" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#largeModal">
                                    <img src="<?php echo e(asset('img/confirm.png')); ?>" alt="" width="40">
                                </a>
                            </td>
                            <td>
                                <?php echo e($user->name); ?>

                            </td>
                            <td>
                                <?php echo e($solicitacoes->tipo); ?>

                            </td>
                            <td>
                                <?php echo e(date('d/m/Y', strtotime($solicitacoes->created_at))); ?>

                            </td>
                            <td>
                                <?php
                                $datacri = $solicitacoes->created_at;
                                echo date('d/m/y', strtotime($datacri.'+ 20 days'));
                                ?>
                            </td>
                        </tr>
                        </tbody>
                    <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>