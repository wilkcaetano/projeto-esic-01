<?php $__env->startSection('content'); ?>

    <style>

        #point{

            cursor: pointer;

        }

    </style>

    <div class="col-md-12" style="background: white;">

        <div class="box-header">

            <h3 class="box-title">Solicitações Recentes</h3>

        </div>

        <!-- /.box-header -->

        <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">

                <thead>

                <tr>

                    <th>Estado</th>

                    <th>Nome Solicitante</th>

                    <th>Tipo</th>

                    <th>Data da Solicitação</th>

                    <th>Data Resposta</th>

                </tr>

                </thead>

                <?php $__currentLoopData = $solicitacao->sortByDesc('id'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $solicitacoes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <?php if($user->id == $solicitacoes->id_users): ?>

                    <?php if($solicitacoes->status == 'NR'): ?>

                        <tbody>

                        <tr>

                            <td>

                                <a href="<?php echo e(route('abrir', $solicitacoes->id)); ?>">

                                    <img src="<?php echo e(asset('img/mail_closed.png')); ?>" alt="" width="40" id="point" >

                                </a>

                            </td>

                            <td>

                                <?php echo e($user->name); ?>




                            </td>

                            <td>

                                <?php echo e($solicitacoes->tipo); ?>


                            </td>

                            <td>

                                <?php echo e(date('d/m/Y', strtotime($solicitacoes->created_at))); ?>


                            </td>

                            <td>

                                <?php

                                $datacri = $solicitacoes->created_at;

                                echo date('d/m/y', strtotime($datacri.'+ 20 days'));

                                ?>

                            </td>

                        </tr>

                        </tbody>

                                <script>

                                    function alerta() {

                                        $("a").click(function(evento) {

                                            evento.preventDefault();

                                            if (confirm("Tem certeza?"))

                                                window.location.replace($(this).attr('href'));

                                        });

                                    }

                                    var link = "<?php echo e(route('abrir', $solicitacoes->id)); ?>";

                                    function confirmExclusao() {

                                        if (confirm("Tem certeza que deseja abrir essa solicitação? Quando abrir será enviado para o Solicitante um email com toda tramitação, inclusive com o nome de quem abriu.")) {

                                            location.href= link;

                                        }

                                    }

                                </script>

                    <?php endif; ?>

                        <?php endif; ?>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </table>



        </div>

        <!-- /.box-body -->

    </div>

    <!-- /.box -->

    </div>

    <!-- /.col -->

    </div>

    <!-- /.row -->

    </section>

    <!-- /.content -->

    </div>



    <script>

        $(function () {

            $('#example1').DataTable()

            $('#example2').DataTable({

                'paging'      : true,

                'lengthChange': false,

                'searching'   : false,

                'ordering'    : true,

                'info'        : true,

                'autoWidth'   : false

            })

        })

    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.painel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>