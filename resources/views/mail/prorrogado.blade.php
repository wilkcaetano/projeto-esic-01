<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <title>Envio de Email</title>

</head>
<body>
<div class="container">
    <img src="http://esic.prodatta.com/public/img/esic_livre.png" alt="">
    <img src="http://esiccondado.acaotransparencia.com.br/img/logomarca-condado.png" alt="" style="width: 300px;position: relative;float: right;">
    <div class="col-md-12">
        <h2>Solicitação Prorrogada</h2>
    </div>
    <div class="col-md-12" id="conteudo">
        <p>
            Sua solicitação com a mensagem <i style="color:royalblue;">{{ $soli->mensagem }}</i> foi prorrogada pelo Administrador do Sistema, quando prorrogada a solicitação<br>
            A Câmara terá mais 10 dias para responder a solicitação. Data Prevista para resposta: <?php $dataCriacao = $soli->created_at;  echo date('d/m/y', strtotime($dataCriacao.'+ 30 days'));?>.

        </p>
    </div>
</div>
</body>
</html>

