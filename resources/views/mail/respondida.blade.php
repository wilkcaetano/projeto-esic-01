<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <title>Envio de Email</title>
</head>
<body>
<div class="container">
    <img src="http://esic.prodatta.com/public/img/esic_livre.png" alt="">
    <img src="http://esiccondado.acaotransparencia.com.br/img/logomarca-condado.png" alt="" style="width: 300px;position: relative;float: right;">
    <div class="col-md-12">
        <h2>Solicitação Respondida</h2>
    </div>
    <div class="col-md-12" id="conteudo">
        <p>
            Sua Solicitação com a mensagem <i style="color: darkblue;">{{ $soli->mensagem }}</i> foi respondida no sistema. <br>
            Obrigado por entrar em contato, caso a resposta não seja satisfatória entre em contato
            conosco para maiores esclarecimentos.<br>
            <?php

                if($resp->status == 'PR'){

                $datafinal = new DateTime('+ 30 days'.$soli->created_at);

            }else{

                    $datafinal = new DateTime('+ 20 days'.$soli->created_at);

                }

            $dataatual = new DateTime();

            date_default_timezone_set('America/Sao_Paulo');

            $total = $dataatual->diff($datafinal);

            $result = $total->format('%R%a Dias');

            if ($result > 1){

                echo "Sua solicitação foi respondida dentro do prazo estipulado por lei prazo positivo de :".$result;

            }else{

                echo "Sua solicitação foi respondida fora do prazo estipulado por lei prazo negatino de :".$result;

            }



            ?>

        </p>
    </div>
</div>
</body>
</html>

