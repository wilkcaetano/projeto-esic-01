<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <title>Envio de Email</title>
</head>
<body>
<div class="container">
    <img src="http://esic.prodatta.com/public/img/esic_livre.png" alt="">
    <img src="http://esiccondado.acaotransparencia.com.br/img/logomarca-condado.png" alt="" style="width: 300px;position: relative;float: right;">
    <div class="col-md-12">
        <h2>{{ "Olá $nome " }}</h2>
    </div>
    <div class="col-md-12" id="conteudo">
        <p>
            Seja muito bem vindo a sistema e-SIC da Câmara Municipal de Machados agora você é {{ $cargo }} <br> entre pelo link: <a href="http://sistema.machados.acaotransparencia.com.br/admin/login">Aqui</a> <br> Com as Credenciais: <br> Usuário: {{ $email }} <br> Senha: {{ $senha }}
        </p>
    </div>
</div>
</body>
</html>

