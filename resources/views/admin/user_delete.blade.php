@extends('admin.painel')

@section('content')

    <div class="col-md-12">
        <div class="col-md-12">
            <aside class="profile-nav alt">
                <section class="card">
                    <div class="card-header user-header alt " style="background: red;">
                        <div class="media">
                            <a href="#">
                                <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{ url('public/img/avatar-homem.jpg') }}">
                            </a>
                            <div class="media-body">
                                <h2 class="text-light display-6">{{ $user->name }}</h2>
                                <p style="color: white;">Tem Certeza Que Deseja excluir esse Usuário?</p>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <spam class="cor">Sexo:</spam> {{ $user->sexo }}
                        </li>
                        <li class="list-group-item">
                            <spam class="cor">Profissão: </spam> {{ $user->profissao }}
                        </li>
                        <li class="list-group-item">
                            <spam class="cor">Faixa Etária: </spam> {{ $user->faixa_etaria }}
                        </li>
                        <li class="list-group-item">
                            <spam class="cor"> Escolaridade: </spam> {{ $user->escolaridade }}
                        </li>
                        <li class="list-group-item">
                            <spam class="cor"> Rua: </spam> {{ $user->rua }} nº: {{ $user->numero }}
                        </li>
                        <li class="list-group-item">
                            <spam class="cor"> CEP: </spam> {{ $user->cep }}
                        </li>
                        <li class="list-group-item">
                            <a href="javascript:history.back()" class="btn btn-primary">Voltar</a>
                            <a href="{{ route('destroy', $user->id) }}" class="btn btn-danger">
                                Deletar
                            </a>
                        </li>
                    </ul>

                </section>
            </aside>
        </div>
    </div>

@stop
