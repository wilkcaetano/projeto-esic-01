@extends('admin.painel')



@section('content')

    <style>

        #point{

            cursor: pointer;

        }

    </style>

    <div class="col-md-12" style="background: white;">

        <div class="box-header">

            <h3 class="box-title">Solicitações Recentes</h3>

        </div>

        <!-- /.box-header -->

        <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">

                <thead>

                <tr>

                    <th>Estado</th>

                    <th>Nome Solicitante</th>

                    <th>Tipo</th>

                    <th>Data da Solicitação</th>

                    <th>Data Resposta</th>

                </tr>

                </thead>

                @foreach($solicitacao->sortByDesc('id') as $solicitacoes)

                    @foreach($users as $user)

                        @if($user->id == $solicitacoes->id_users)

                    @if($solicitacoes->status == 'NR')

                        <tbody>

                        <tr>

                            <td>

                                <a href="{{ route('abrir', $solicitacoes->id) }}">

                                    <img src="{{ asset('img/mail_closed.png') }}" alt="" width="40" id="point" >

                                </a>

                            </td>

                            <td>

                                {{ $user->name }}



                            </td>

                            <td>

                                {{ $solicitacoes->tipo }}

                            </td>

                            <td>

                                {{ date('d/m/Y', strtotime($solicitacoes->created_at)) }}

                            </td>

                            <td>

                                <?php

                                $datacri = $solicitacoes->created_at;

                                echo date('d/m/y', strtotime($datacri.'+ 20 days'));

                                ?>

                            </td>

                        </tr>

                        </tbody>

                                <script>

                                    function alerta() {

                                        $("a").click(function(evento) {

                                            evento.preventDefault();

                                            if (confirm("Tem certeza?"))

                                                window.location.replace($(this).attr('href'));

                                        });

                                    }

                                    var link = "{{ route('abrir', $solicitacoes->id) }}";

                                    function confirmExclusao() {

                                        if (confirm("Tem certeza que deseja abrir essa solicitação? Quando abrir será enviado para o Solicitante um email com toda tramitação, inclusive com o nome de quem abriu.")) {

                                            location.href= link;

                                        }

                                    }

                                </script>

                    @endif

                        @endif

                    @endforeach

                @endforeach

            </table>



        </div>

        <!-- /.box-body -->

    </div>

    <!-- /.box -->

    </div>

    <!-- /.col -->

    </div>

    <!-- /.row -->

    </section>

    <!-- /.content -->

    </div>



    <script>

        $(function () {

            $('#example1').DataTable()

            $('#example2').DataTable({

                'paging'      : true,

                'lengthChange': false,

                'searching'   : false,

                'ordering'    : true,

                'info'        : true,

                'autoWidth'   : false

            })

        })

    </script>

@stop
