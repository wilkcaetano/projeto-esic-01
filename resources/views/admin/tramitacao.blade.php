@extends('admin.painel')



@section('content')

        <div class="col-md-12" style="background: white;">

            <table id="example1" class="table table-bordered table-striped">

            <thead>

                <tr>

                <th>Identificação da Solicitação</th>

                <th>Nome Solicitante</th>

                <th>Resposta Usuário</th>

                <th>Resposta Administrador</th>

                <th>Data da Solicitação</th>

                <th>Data Resposta</th>

                <th>Anexo da Solicitação</th>

                <th>Anexo da Resposta</th>

                </tr>

            </thead>

            @foreach($resposta as $respostas)

                @if($respostas->id_solicitacao == $solicitacao->id)

                <tbody>

                    <tr>

                    <td>

                      {{ $solicitacao->id }}

                    </td>

                    <td>

                        {{ $user->name }}

                    </td>

                    <td>

                        {{ $solicitacao->mensagem }}

                    </td>

                    <td>

                        {{ $respostas->resposta }}

                    </td>

                    <td>

                        {{ date('d/m/Y', strtotime($solicitacao->created_at)) }}

                    </td>

                    <td>

                        {{ date('d/m/Y', strtotime($respostas->created_at)) }}

                    </td>

                    <td>

                        @if(empty($solicitacao->arquivo))

                            {{ "Não Consta" }}

                        @else

                            {{ $solicitacao->arquivo}}

                        @endif

                    </td>

                    <td>

                        @if(empty($respostas->arquivo_resposta))

                            {{ "Não Consta" }}

                        @else
                            <a href="{{ url('public/uploads/arquivos/respostas/'.$respostas->id_users.'/'.$respostas->arquivo_resposta) }}" target="_blank">
                                {{ $respostas->arquivo_resposta}}
                            </a>
                        @endif

                    </td>

                    </tr>

                </tbody>

                @endif

            @endforeach

            </table>

        </div>

@stop
