@extends('admin.painel')



@section('content')

    <?php

        $data = date('Y');

    ?>

    <section class="statistic">

        <div class="section__content section__content--p30">

            <div class="container-fluid">

                <div class="row">

                    <div class="col-md-6 col-lg-3">

                        <a href="{{ route('usuarios') }}">

                        <div class="statistic__item">

                            <h2 class="number">

                                <?php

                                    $select = \Illuminate\Support\Facades\DB::select('select * from users');

                                    echo count($select);

                                ?>

                            </h2>

                            <span class="desc">usuários cadastrados</span>

                            <div class="icon">

                                <i class="zmdi zmdi-account-o"></i>

                            </div>

                        </div>

                        </a>

                    </div>

                    <div class="col-md-6 col-lg-3">

                        <div class="statistic__item">

                            <h2 class="number">

                                <?php

                                    $select = \Illuminate\Support\Facades\DB::select('select * from solicitacaos');

                                    echo count($select);

                                ?>

                            </h2>

                            <span class="desc">Total de Solicitações</span>

                            <div class="icon">

                                <i class="zmdi zmdi-shopping-cart"></i>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-6 col-lg-3">

                        <div class="statistic__item">

                            <h2 class="number">

                                <?php

                                $select = \Illuminate\Support\Facades\DB::select("select * from solicitacaos where status = 'NR'");

                                echo count($select);

                                ?>

                            </h2>

                            <span class="desc">Solicitações Recentes</span>

                            <div class="icon">

                                <i class="zmdi zmdi-calendar-note"></i>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-6 col-lg-3">

                        <div class="statistic__item">

                            <h2 class="number">

                                <?php

                                foreach ($solicitacao as $solicitacoes){

                                    $id =  $solicitacoes->id;

                                    if ($solicitacoes->status == "PR"){

                                        $datafinal = new DateTime ('+ 30 days'.$solicitacoes['created_at']);

                                        $dataatual = new DateTime();

                                        //date_default_timezone_set('America/Sao_Paulo');

                                        $total = date_diff($datafinal, $dataatual);

                                        $result = $total->format('%d Dias');

                                        if ($result < 5){

                                            \Illuminate\Support\Facades\DB::table('solicitacaos')->where('id', $id)->update(['vencimento' => 'Sim']);

                                            $contador = count($solicitacoes->vencimento == 'Sim');

                                            echo "<script>window.alert('Tem $contador solicitação(s) perto do vencimento! Foi prorrogada.')</script>";

                                            echo $contador;

                                        }

                                    }

                                    if($solicitacoes->status == "NR" || $solicitacoes->status == "AR"){

                                        $datafinal = new DateTime ('+ 20 days'.$solicitacoes['created_at']);

                                        $dataatual = new DateTime();



                                        //date_default_timezone_set('America/Sao_Paulo');

                                        $total = $dataatual->diff($datafinal);

                                        $result = $total->format('%R%a');

                                        if ($result < 5){

                                            \Illuminate\Support\Facades\DB::table('solicitacaos')->where('id', $id)->update(['vencimento' => 'Sim']);

                                            $contador = count($solicitacoes->vencimento == 'Sim');

                                            echo "<script>window.alert('Tem $contador solicitação(s) perto do vencimento! Não foi prorrogada.')</script>";

                                            echo $contador;

                                        }

                                    }







                                }

                                ?>

                            </h2>

                            <span class="desc">Perto do vencimento</span>

                            <div class="icon">

                                <i class="zmdi zmdi-money"></i>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!------------------New Message --------------------->







        <!--------------------------End New message ------------->

        <div class="col-lg-6" style="position: relative; float: left;">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <h3 class="title-2 m-b-40">Solicitações Realizadas e Respondidas</h3>
                    <canvas id="barChart"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-6" style="position: relative; float: left;">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <h3 class="title-2 m-b-40">Prioridade da Solicitação</h3>
                    <canvas id="doughutChart"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-6" style="position: relative; float: left;">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <h3 class="title-2 m-b-40">Faixa Etária</h3>
                    <canvas id="pieChart"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-6" style="position: relative; float: left;">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <h3 class="title-2 m-b-40">Escolaridade</h3>
                    <canvas id="polarChart"></canvas>
                </div>
            </div>
        </div>
    </section>



@endsection

