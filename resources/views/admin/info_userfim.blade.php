@extends('admin.painel')
@section('content')
    <style>
        .cor{
            color: #1f6fb2;
        }
    </style>
    <div class="col-md-12">
        <div class="col-md-12">
            <aside class="profile-nav alt">
                <section class="card">
                    <div class="card-header user-header alt bg-dark">
                        <div class="media">
                            <a href="#">
                                @if($user->sexo == "Masculino")
                                    <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{ url('public/img/avatar-homem.jpg') }}">
                                @elseif($user->sexo == "")
                                    <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{ url('public/img/avatar-homem.jpg') }}">
                                @else
                                    <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{ url('public/img/avatar-mulher.jpg') }}">
                                @endif

                            </a>
                            <div class="media-body">
                                <h2 class="text-light display-6">{{ $user->name }}</h2>
                                <p>Cidadão Solicitante</p>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <spam class="cor">Sexo:</spam> {{ $user->sexo }}
                        </li>
                        <li class="list-group-item">
                            <spam class="cor">Profissão: </spam> {{ $user->profissao }}
                        </li>
                        <li class="list-group-item">
                            <spam class="cor">Faixa Etária: </spam> {{ $user->faixa_etaria }}
                        </li>
                        <li class="list-group-item">
                            <spam class="cor"> Escolaridade: </spam> {{ $user->escolaridade }}
                        </li>
                        <li class="list-group-item">
                            <spam class="cor"> Rua: </spam> {{ $user->rua }} nº: {{ $user->numero }}
                        </li>
                        <li class="list-group-item">
                            <spam class="cor"> CEP: </spam> {{ $user->cep }}
                        </li>
                        <li class="list-group-item">
                            <a href="javascript:history.back()" class="btn btn-primary">Voltar</a> <a href="{{route('delete', $user->id)}}" class="btn btn-danger">Excluir Usuário</a>
                        </li>
                    </ul>

                </section>
            </aside>
        </div>
    </div>
@stop
