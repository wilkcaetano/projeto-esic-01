@extends('admin.painel')
@section('content')
    <div class="col-md-12" >
        <div class="container" style="margin-top: 80px;height: 100%;">
            <div class="col-md-12">
                <h3 class="title-5 m-b-35">lista de ADM's cadastrados no sistema</h3>
                <div class="table-responsive table-responsive-data2">
                    <table class="table table-data2">
                        <thead>
                        <tr>
                            <th>nome</th>
                            <th>email</th>
                            <th>cargo</th>
                            <th>data de cadastro</th>
                        </tr>
                        </thead>
                        @foreach($admin as $admins)
                            <tbody style="background: #edecec;">
                            <tr class="tr-shadow">
                                <td>{{ $admins->name }}</td>
                                <td>
                                    <span class="block-email">{{ $admins->email }}</span>
                                </td>
                                <td class="desc">{{ $admins->cargo }}</td>
                                <td>
                                    <span class="status--process">{{ date('d/m/Y', strtotime($admins->created_at)) }}</span>
                                </td>
                                <td>
                                    <div class="table-data-feature">
                                        <a href="" class="item" data-toggle="tooltip" data-placement="top" title="Chat Mensagens Curtas">
                                            <i class="zmdi zmdi-mail-send"></i>
                                        </a>
                                        <a href="{{ route('delet-adm', $admins->id) }}" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                        <a href="{{ route('info-adm', $admins->id) }}" class="item" data-toggle="tooltip" data-placement="top" title="Mais Informações">
                                            <i class="zmdi zmdi-more"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr class="spacer"></tr>
                            </tr>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop
