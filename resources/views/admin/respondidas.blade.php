@extends('admin.painel')



@section('content')

    <div class="box" style="background: white;">

        <div class="box-header">

            <h3 class="box-title">Solicitações Que Foram Respondidas!</h3>

        </div>

        <!-- /.box-header -->

        <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">

                <thead>

                <tr>

                    <th>Estado</th>

                    <th>Nome Solicitante</th>

                    <th>Tipo</th>

                    <th>Data da Solicitação</th>



                </tr>

                </thead>

                @foreach($solicitacao as $solicitacoes)

                    @foreach($users as $user)

                        @if($user->id == $solicitacoes->id_users)

                    @if($solicitacoes->status == 'RP')

                        <tbody>

                        <tr>

                            <td>

                                <a href="{{ route('tramitacao', $solicitacoes->id) }}">

                                    <img src="{{ asset('img/confirm.png') }}" alt="" width="40">

                                </a>

                            </td>

                            <td>

                                {{ $user->name }}

                            </td>

                            <td>

                                {{ $solicitacoes->tipo }}

                            </td>

                            <td>

                                {{ date('d/m/Y', strtotime($solicitacoes->created_at)) }}

                            </td>

                        </tr>

                        </tbody>

                        @endif

                        @endif

                    @endforeach

                @endforeach

            </table>

        </div>

        <!-- /.box-body -->

    </div>

    <!-- /.box -->

    </div>

    <!-- /.col -->

    </div>

    <!-- /.row -->

    </section>

    <!-- /.content -->

    </div>

    <script>

        $(function () {

            $('#example1').DataTable()

            $('#example2').DataTable({

                'paging'      : true,

                'lengthChange': false,

                'searching'   : false,

                'ordering'    : true,

                'info'        : true,

                'autoWidth'   : false

            })

        })

    </script>

@stop
