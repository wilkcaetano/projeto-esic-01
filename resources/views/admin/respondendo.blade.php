@extends('admin.painel')

@section('content')

    <div class="col-md-12" style=";background: white;">

        <div class="col-md-12" style="margin-bottom: 50px;">

            @if($solicitacao->status == 'PR')

                <h2>Responder a prorrogação da solicitação de {{ $user->name }}.</h2>

            @else

                <h2>Responder a Solcitação de {{ $user->name }}</h2>

            @endif

        </div>

        <form action="{{ route('resposta', $solicitacao->id) }}" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}

            <input type="hidden" name="id_solicitacao" value="{{ $solicitacao->id }}">

            <input type="hidden" name="id_users" value="{{ $user->id }}">

            <input type="hidden" name="adm" value="{{ Auth::user()->name }}">

            <input type="hidden" name="email" value="{{ $user->email }}">

                @if($solicitacao->status == 'PR')

                <input type="hidden" name="prorrogar" value="Não">

                @else

                    <div class="col-md-6" style="position: relative; float: left;">

                            <label>Prorrogar?</label>

                            <select name="prorrogar" id="" class="form-control">

                                <option value=""></option>

                                <option value="Sim">Sim</option>

                                <option value="Não">Não</option>

                            </select>

                    </div>

                @endif

                <div class="col-md-6" style="position: relative; float: left;">

                    <label>Tipo da Resposta</label>

                    <select name="tipo_resposta" id="" class="form-control">

                        <option value=""></option>

                        <option value="Responder">Responder</option>

                        <option value="Adiar">Adiar</option>

                        <option value="Justificar Prorrogação">Justificar Prorrogação</option>

                    </select>

                </div>

            <di class="col-md-12" style="position: relative; float: left;">

                <label>Resposta para Solicitação</label>

                <textarea  name="resposta" class="form-control" style="height: 350px; padding: 10px;"></textarea>

            </di>

            <div class="col-md-12" style="position: relative; float: left;">

                <label>Enviar Um Arquivo</label> <br>

                <input  type="file" name="arquivo_resposta">

            </div>

            <div class="col-md-12" style="position: relative; float: left;margin-top: 20px;margin-bottom: 20px;">

               <input class="btn btn-success" type="submit" value="Enviar Resposta">

            </div>

        </form>

    </div>

@stop
