@extends('admin.painel')



@section('content')

    <div class="box">

        <div class="box-header">

            <h3 class="box-title">Solicitações Que Foram Prorrogadas!</h3>

        </div>

        <!-- /.box-header -->

        <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">

                <thead>

                <tr>

                    <th>Estado</th>

                    <th>Nome Solicitante</th>

                    <th>Tipo</th>

                    <th>Data da Solicitação</th>

                    <th>Data Resposta</th>

                </tr>

                </thead>

                @foreach($solicitacao->sortByDesc('id') as $solicitacoes)

                    @foreach($users as $user)

                        @if($user->id == $solicitacoes->id_users)

                    @if($solicitacoes->status == 'PR')

                        <tbody>

                        <tr>

                            <td>

                                <a href="{{ route('abrir', $solicitacoes->id) }}">

                                    <img src="{{ asset('img/incorrect.png') }}" alt="" width="40">

                                </a>

                            </td>

                            <td>

                                {{ $user->name }}

                            </td>

                            <td>

                                {{ $solicitacoes->tipo }}

                            </td>

                            <td>

                                {{ date('d/m/Y', strtotime($solicitacoes->created_at)) }}

                            </td>

                            <td>

                                <?php

                                $datacri = $solicitacoes->created_at;

                                echo date('d/m/y', strtotime($datacri.'+ 30 days'));

                                ?>

                            </td>

                        </tr>

                        </tbody>

                    @endif

                        @endif

                   @endforeach

                @endforeach

            </table>

        </div>

        <!-- /.box-body -->

    </div>

@stop
