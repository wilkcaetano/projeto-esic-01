@extends('admin.painel')
@section('content')
    <style>
        .cor{
            color: #1f6fb2;
        }
    </style>
    <div class="col-md-12">
        <div class="col-md-12">
            <aside class="profile-nav alt">
                <section class="card">
                    <div class="card-header user-header alt" style="background: darkred;">
                        <div class="media">
                            <a href="#">
                                @if($adm->sexo == "Masculino")
                                    <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{ url('public/img/avatar-homem.jpg') }}">
                                @elseif($adm->sexo == "")
                                    <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{ url('public/img/avatar-homem.jpg') }}">
                                @else
                                    <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{ url('public/img/avatar-mulher.jpg') }}">
                                @endif
                            </a>
                            <div class="media-body">
                                <h2 class="text-light display-6">{{ $adm->name }}</h2>
                                <p style="color: white;">Tem certeza que quer excluir esse {{ $adm->cargo }}</p>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <spam class="cor">ID:</spam> {{ $adm->id }}
                        </li>
                        <li class="list-group-item">
                            <spam class="cor">Email: </spam> {{ $adm->email }}
                        </li>
                        <li class="list-group-item">
                            <a href="javascript:history.back()" class="btn btn-primary">Voltar</a> <a href="{{route('destroy-adm', $adm->id) }}" class="btn btn-danger">Excluir Usuário</a>
                        </li>
                    </ul>

                </section>
            </aside>
        </div>
    </div>
@stop
