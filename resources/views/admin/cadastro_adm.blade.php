@extends('admin.painel')
@section('content')
    <div class="col-lg-6" style="margin: 0 auto; margin-bottom: 20px;background: white;">
        @if(Session::has('message'))
            <div id="msg" class="alert alert-danger" style="text-align: center;">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif  
        <div class="card">
            <div class="card-header">Cadastro de Administradores</div>
            <div class="card-body card-block">
                <form action="{{ action('AdminController@cadastroadm') }}" method="post" class="">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Nome</div>
                            <input type="text" id="username3" name="name" class="form-control">
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Email</div>
                            <input type="email" id="email3" name="email" class="form-control">
                            <div class="input-group-addon">
                                <i class="fa fa-envelope"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Cargo</div>
                            <select name="cargo" id="" class="form-control">
                                <option value=""></option>
                                <option value="Admin">Administrador</option>
                                <option value="Editor">Editor</option>
                            </select>
                            <div class="input-group-addon">
                                <i class="fa fa-briefcase"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Sexo</div>
                            <select name="sexo" id="" class="form-control">
                                <option value=""></option>
                                <option value="Feminino">Feminino</option>
                                <option value="Masculino">Masculino</option>
                            </select>
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Senha</div>
                            <input type="password" id="password3" name="password" class="form-control">
                            <div class="input-group-addon">
                                <i class="fa fa-asterisk"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions form-group">
                        <button type="submit" class="btn btn-primary btn-sm">Cadastrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
