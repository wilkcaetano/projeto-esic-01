@extends('painel.painel')



@section('content')

    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">etapas percorridas pela solicitação</h3>
        <div class="table-data__tool">
            <div class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead>
                    <tr>
                        @if($solicitacao->status != 'NR')
                            <th>Administrador da Resposta</th>
                            <th>Tipo de Resposta</th>
                            <th>mensagem</th>
                            <th>Data de Resposta</th>
                            <th>status</th>
                            <th>resposta</th>
                            <th>anexo do administrador</th>
                        @endif
                    </tr>
                    </thead>
                        @foreach($resposta as $respostas)
                            @if($respostas->id_solicitacao == $solicitacao->id)
                            <tbody>
                            <tr class="tr-shadow">
                                <td>
                                    {{ $respostas->adm }}
                                </td>
                                <td>
                                    @if($respostas->resposta == 'Aberto')
                                        {{ "Não Consta" }}
                                    @else
                                        {{ $respostas->tipo_resposta }}
                                    @endif
                                </td>
                                <td>
                                    {{ $solicitacao->mensagem }}
                                </td>
                                <td class="desc">
                                    @if($respostas->resposta == 'Aberto')
                                        {{ date('d/m/Y', strtotime($respostas->created_at)) }}
                                    @else
                                        {{ date('d/m/Y', strtotime($respostas->created_at)) }}
                                    @endif
                                </td>
                                <td>
                                   @if($respostas->status == 'AR')
                                        <spam style="color: #00a2e3;">
                                            {{ "Foi Aberta" }}
                                        </spam>
                                        @elseif($respostas->status == 'PR')
                                        <spam class="status--denied">
                                            {{ "Foi Prorrogado" }}
                                        </spam>
                                       @elseif($respostas->status == 'RP')
                                        <spam class="status--process">
                                             {{ "Foi Respondido" }}
                                        </spam>
                                   @endif
                                </td>
                                <td>
                                    @if($solicitacao->id == $respostas->id_solicitacao)
                                        @if($respostas->resposta == 'Aberto')
                                            {{ "Não Consta" }}
                                        @else
                                            {{ $respostas->resposta }}
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($respostas->arquivo_resposta))
                                        <a href="{{ url('public/uploads/arquivos/respostas/'.$respostas->id_users.'/'.$respostas->arquivo_resposta) }}">Anexo</a>
                                        @else
                                        {{ "Não Consta" }}
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                            @endif
                        @endforeach
                    @if($solicitacao->status == 'NR')
                        <tbody>
                            {{ "Não Consta Respostas" }}
                        </tbody>
                    @endif
                </table>
            </div>
            <!-- END DATA TABLE -->
        </div>
    </div>

@stop
