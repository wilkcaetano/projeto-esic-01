@extends('painel.painel')

@section('content')
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">solicitações realizadas</h3>
        <div class="table-data__tool">
            <div class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead>
                    <tr>
                        <th>prioridade</th>
                        <th>tipo</th>
                        <th>mensagem</th>
                        <th>data de criação</th>
                        <th>status</th>
                        <th>Tramitação</th>
                    </tr>
                    </thead>
                    @foreach($solicitacao->sortByDesc('id') as $solicitacoes)
                        @if(Auth::user()->id == $solicitacoes->id_users)
                            <tbody>
                            <tr class="tr-shadow">
                                <td>
                                    {{ $solicitacoes->prioridade }}
                                </td>
                                <td>
                                    {{ $solicitacoes->tipo }}
                                </td>
                                <td class="desc">
                                    {{ $solicitacoes->mensagem }}
                                </td>
                                <td>
                                    {{ date('d/m/Y H:i:s', strtotime($solicitacoes->created_at)) }}
                                </td>
                                <td>

                                    @if($solicitacoes->status == 'AR')
                                        <spam style="color: #00a2e3;">
                                            {{ "Foi Aberta" }}
                                        </spam>
                                    @elseif($solicitacoes->status == 'PR')
                                        <spam class="status--denied">
                                            {{ "Foi Prorrogado" }}
                                        </spam>
                                    @elseif($solicitacoes->status == 'RP')
                                        <spam class="status--process">
                                            {{ "Foi Respondido" }}
                                        </spam>
                                    @elseif($solicitacoes->status == 'NR')
                                        <spam class="status--denied">
                                            {{ "Não Respondido" }}
                                        </spam>
                                    @endif

                                </td>
                                <td>
                                    <div class="table-data-feature">
                                        @if($solicitacoes->status != 'NR')
                                        <a href="{{ route('user_tramitacao', $solicitacoes->id) }}" class="item" data-toggle="modal">
                                            <i class="zmdi zmdi-more"></i>
                                        </a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        @endif
                    @endforeach
                </table>
            </div>
            <!-- END DATA TABLE -->
        </div>
    </div>
@stop