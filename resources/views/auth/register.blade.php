@extends('painel.register')
@section('content')
<div class="page-wrapper" >
    <div class="page-content" style="background: #edecec; min-height: 300px; overflow: hidden;">
        <div id="linha-menu" class="col-md-12" style="min-height:50px;overflow: hidden;">
            <div id="menu-central" class="container" >
                <div class="navbar navbar-expand-md navbar-dark bg-dark" style="background-color:rgba(0,0,0, 0.0)!important;border-bottom: 1px solid gainsboro;">
                    <div class="container" style="color:#000000;"><span class="text-white d-md-none" style="color:rgb(0,0,0)!important;">e-Sic Câmara</span><button class="btn btn-link navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav"><i class="fa fa-align-justify" style="color:rgba(0,0,0,0.5);"></i></button>
                        <div
                                id="main-nav" class="navbar-collapse collapse">
                            <ul class="navbar-nav nav-fill w-100">
                                <li class="nav-item col-md-2"><a href="{{ url('/') }}" class="nav-link"  style="color:rgb(0,0,0);">Home</a></li>
                                <a href="{{ route('graficos') }}" class="nav-link col-md-2" style="color:rgb(0,0,0);"> <li class="nav-item">Gráficos e Estatísticas</li></a>
                                <a href="{{ route('login') }}" class="nav-link col-md-2" style="color:rgb(0,0,0);"> <li class="nav-item">Login</li></a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="padding: 20px;">
            <div class="login-wrap" style="margin-top: -60px;">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="#">
                            <img src="{{ asset('img/esic_livre.png') }}" alt="CoolAdmin">
                        </a>
                    </div>
                    <span style="color: darkred; font-size: 12px;margin-left: 20%;">Os campos que estriverem com * são campos obrigatórios</span>
                    <div class="login-form">
                        <form method="POST" action="{{ route('registrar') }}" aria-label="{{ __('Register') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Nome Completo *</label>
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nome') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Email Address *</label>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Profissão</label>
                                <input id="profissao" type="text" class="form-control{{ $errors->has('profissao') ? ' is-invalid' : '' }}" name="profissao" value="{{ old('profissao') }}" >
                                @if ($errors->has('profissao'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('profissao') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12" style="padding: 0;">
                                <div class="col-md-6  form-group"
                                     style="position: relative; float: left;margin-left:0; ">
                                    <label>Escolaridade</label>
                                    <select name="escolaridade" id="" class="form-control">
                                        <option value=""></option>
                                        <option value="Ensino Fundamental">Ensino Fundamental</option>
                                        <option value="Ensino Médio">Ensino Médio</option>
                                        <option value="Ensino Superior">Ensino Superior</option>
                                        <option value="Mestrado/Doutorado">Mestrado/Doutorado</option>
                                        <option value="Sem Formação">Sem Formação</option>
                                    </select>
                                    @if ($errors->has('nome'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nome') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-6 form-group"
                                     style="position: relative; float: left;margin-right: 0;">
                                    <label>Faixa Etária</label>
                                    <select name="faixa_etaria" id="" class="form-control">
                                        <option value=""></option>
                                        <option value="Acima de 59 anos">Acima de 59 anos</option>
                                        <option value="De 41 a 59 anos">De 41 a 59 anos</option>
                                        <option value="Entre 21 e 40 anos">Entre 21 e 40 anos</option>
                                        <option value="Até 20 anos">Até 20 anos</option>
                                        <option value="Menos de 20 anos">Menos de 20 anos</option>
                                    </select>
                                    @if ($errors->has('nome'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nome') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Sexo</label>
                                <select name="sexo" id="" class="form-control">
                                    <option value=""></option>
                                    <option value="Feminino">Feminino</option>
                                    <option value="Masculino">Masculino</option>
                                    <option value="Prefiro Não Responder">Prefiro Não Responder</option>

                                </select>
                                @if ($errors->has('sexo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('sexo') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Rua</label>
                                <input id="rua" type="text"
                                       class="form-control{{ $errors->has('rua') ? ' is-invalid' : '' }}" name="rua"
                                       value="{{ old('rua') }}">
                                @if ($errors->has('rua'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('rua') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12" style="padding: 0;">
                                <div class="col-md-6 form-group" style="position: relative; float: left;">
                                    <label>CEP</label>
                                    <input id="cep" type="text"
                                           class="form-control{{ $errors->has('cep') ? ' is-invalid' : '' }}" name="cep"
                                           value="{{ old('cep') }}" onkeypress="mascara(this, '#####-###')" maxlength="9">
                                    @if ($errors->has('cep'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cep') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="col-md-6 form-group" style="position: relative; float: left;">
                                    <label>Número</label>
                                    <input id="numero" type="text"
                                           class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }}"
                                           name="numero" value="{{ old('numero') }}">
                                    @if ($errors->has('numero'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('numero') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Senha *</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Confirmar Senha *</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                            <input type="submit"  class="au-btn au-btn--block au-btn--green m-b-20" value="Cadastrar">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<script type="text/javascript">
    function mascara(t, mask){
        var i = t.value.length;
        var saida = mask.substring(1,0);
        var texto = mask.substring(i)
        if (texto.substring(0,1) != saida){
            t.value += texto.substring(0,1);
        }
    }
</script>
@endsection
