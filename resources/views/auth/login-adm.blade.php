@extends('painel.login')



@section('content')
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div id="linha-menu" class="col-md-12" style="min-height:50px;overflow: hidden;">
                <div id="menu-central" class="container" >
                    <div class="navbar navbar-expand-md navbar-dark bg-dark" style="background-color:rgba(0,0,0, 0.0)!important;border-bottom: 1px solid gainsboro;">
                        <div class="container" style="color:#000000;"><span class="text-white d-md-none" style="color:rgb(0,0,0)!important;">e-Sic Câmara</span><button class="btn btn-link navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav"><i class="fa fa-align-justify" style="color:rgba(0,0,0,0.5);"></i></button>
                            <div
                                id="main-nav" class="navbar-collapse collapse">
                                <ul class="navbar-nav nav-fill w-100">
                                    <li class="nav-item col-md-2"><a href="{{ url('/') }}" class="nav-link"  style="color:rgb(0,0,0);">Home</a></li>
                                    <a href="{{ route('graficos') }}" class="nav-link col-md-2" style="color:rgb(0,0,0);"> <li class="nav-item">Gráficos e Estatísticas</li></a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <img src="{{ url('img/esic_livre.png') }}" alt="CoolAdmin">
                            </a>
                        </div>
                        <div class="login-form">
                            <form method="POST" action="{{ url('/admin/login') }}" aria-label="{{ __('Login') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="login-checkbox">
                                    <label>
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>Lembre de mim
                                    </label>
                                    <label>
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Esqueceu sua senha?') }}
                                        </a>
                                    </label>
                                </div>
                                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">entrar</button>
                            </form>
                            <div class="register-link">
                                <!--<p>

                                    Don't you have account?

                                    <a href="#">Sign Up Here</a>-->

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection

