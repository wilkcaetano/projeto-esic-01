@extends('painel.painel')



@section('content')
    <div class="col-md-12">
        <!-- DATA TABLE -->
            <h3 class="title-5 m-b-35">solicitações realizadas</h3>
            <div class="table-data__tool">
            <div class="table-responsive table-responsive-data2">
                <table class="table table-data2" style="margin-bottom: 50px;">
                    <thead>
                    <tr>
                        <th>
                        </th>
                        <th>prioridade</th>
                        <th>tipo</th>
                        <th>mensagem</th>
                        <th>data de criação</th>
                        <th>status</th>
                        <th>resposta</th>
                        <th></th>
                    </tr>
                    </thead>
                    @foreach($solicitacao as $solicitacoes)
                        @if(Auth::user()->id == $solicitacoes->id_users)
                    <tbody>
                    <tr class="tr-shadow">
                        <td>
                        </td>
                        <td>
                            {{ $solicitacoes->prioridade }}
                        </td>
                        <td>
                            {{ $solicitacoes->tipo }}
                        </td>
                        <td class="desc">
                            {{ $solicitacoes->mensagem }}
                        </td>
                        <td>
                           {{ date('d/m/Y H:i:s', strtotime($solicitacoes->created_at)) }}
                        </td>
                        <td>
                            @if($solicitacoes->status == 'RP')
                                <span class="status--process">
                                   {{ "Respondido" }}
                                </span>
                            @elseif($solicitacoes->status == 'NR')
                                <span class="status--denied">
                                   {{ "Não Aberta" }}
                                </span>
                            @elseif($solicitacoes->status == 'AR')
                                <span class="status--denied">
                                   {{ "Sendo Analizada" }}
                                </span>
                            @endif
                        </td>
                        <td>
                            Não Consta Resposta
                        </td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="More">
                                    <i class="zmdi zmdi-more"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                        @endif
                    @endforeach
                </table>
            </div>
            <!-- END DATA TABLE -->
        </div>
    </div>
@stop
