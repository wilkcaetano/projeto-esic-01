<!DOCTYPE html>
<html lang="pt-br">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Painel de Controle e-Sic">
    <meta name="author" content="Wilk Caetano">
    <meta name="keywords" content="e-Sic Prodatta">

    <!-- Title Page-->
    <title>Login</title>

    <!-- Fontfaces CSS-->
    <link href="{{ asset('painel/css/font-face.css') }}}" rel="stylesheet" media="all">
    <link href="{{ asset('painel/vendor/font-awesome-4.7/css/font-awesome.min.css') }}}" rel="stylesheet" media="all">
    <link href="{{ asset('painel/vendor/font-awesome-5/css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('painel/vendor/mdi-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ asset('painel/vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{ asset('painel/vendor/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('painel/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('painel/vendor/wow/animate.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('painel/vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('painel/vendor/slick/slick.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('painel/vendor/select2/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('painel/vendor/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{asset('painel/css/theme.css')}}" rel="stylesheet" media="all">

</head>
<body class="animsition">
<!-- modal large -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Cadastro e-SIC</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if(Session::has('message'))
                    <div id="msg" class="alert alert-success" style="text-align: center;">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal large -->
@yield('content')


<!-- Jquery JS-->
<script src="{{ asset('painel/vendor/jquery-3.2.1.min.js') }}"></script>
<!-- Bootstrap JS-->
<script src="{{ asset('painel/vendor/bootstrap-4.1/popper.min.js') }}"></script>
<script src="{{ asset('painel/vendor/bootstrap-4.1/bootstrap.min.js') }}"></script>
<!-- Vendor JS       -->
<script src="{{ asset('painel/vendor/slick/slick.min.js') }}">
</script>
<script src="{{ asset('painel/vendor/wow/wow.min.js') }}"></script>
<script src="{{ asset('painel/vendor/animsition/animsition.min.js') }}"></script>
<script src="{{ asset('painel/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') }}">
</script>
<script src="{{ asset('painel/vendor/counter-up/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('painel/vendor/counter-up/jquery.counterup.min.js') }}">
</script>
<script src="{{ asset('painel/vendor/circle-progress/circle-progress.min.js') }}"></script>
<script src="{{ asset('painel/vendor/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('painel/vendor/chartjs/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('painel/vendor/select2/select2.min.js') }}">
</script>

<!-- Main JS-->
<script src="{{ asset('painel/js/main.js') }}"></script>
</body>
</html>
@if(Session::has('message'))
    <script>
        $(document).ready(function(){
            $("#largeModal").modal();
        })
    </script>
@endif