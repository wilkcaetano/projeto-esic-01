<!DOCTYPE html>

<html>

<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>e-Sic</title>
    <link rel="stylesheet" href="{{ asset('principal/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('principal/assets/fonts/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('principal/assets/fonts/ionicons.min.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Archivo+Black">
    <link rel="stylesheet" href="{{ asset('principal/assets/css/styles.min.css') }}">
    <link rel="stylesheet" href="{{asset('principal/assets/css/efeito.css')}}" media="screen and (max-width: 765px)">
</head>
<body>
<div id="cabecalho" class="row" style="margin:0px;min-height:150px;background-color:#edecec;padding:0;">
    <div id="central-cabecalho" class="container">
        <div id="linha-logo-busca" class="col-md-12" style="height:100px;">
            <div id="espaco" class="d-md-none col-sm-3 col-xs-3" style="height:10px;"></div>
            <div id="lado-logomarca" class="col-md-6 col-sm-6" style="height:100%;"><img src="{{ asset('img/logomarca-condado.png') }}" style="position:sticky;top:10px;width: 250px;"></div>
            <div id="lado-busca" class="col-md-6" style="height:100%;">
                <div id="efeito" class="d-none d-md-none col-sm-2"></div>
                <div id="rede-social" class="col-md-12 col-sm-6 col-xs-6" style="font-size:25px;"><a href="{{ route('login') }}"><i class="icon ion-log-in" style="margin-right:15px;position:relative;"></i></a> <a href="{{ route('register') }}"> <i class="fa fa-user-plus" style="margin-right:15px;"></i></a><a href="{{ url('/admin/login') }}"><i class="fa fa-gears"></i></a></div>
            </div>
        </div>
    </div>
    <div id="linha-menu" class="col-md-12" style="min-height:50px;">
        <div id="menu-central" class="container" >
            <div class="navbar navbar-expand-md navbar-dark bg-dark" style="background-color:#edecec!important;">
                <div class="container" style="color:#000000;"><span class="text-white d-md-none" style="color:rgb(0,0,0)!important;">e-Sic Câmara de Machados</span><button class="btn btn-link navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav"><i class="fa fa-align-justify" style="color:rgba(0,0,0,0.5);"></i></button>
                    <div id="main-nav" class="navbar-collapse collapse">
                        <ul class="navbar-nav nav-fill w-100">
                            <li class="nav-item"><a href="{{ url('/') }}" class="nav-link"  style="color:rgb(0,0,0);">Home</a></li>
                            <li class="nav-item"><a href="#como" class="nav-link" id="scrollLink" style="color:rgb(0,0,0);">Como Solicitar Uma Informação</a></li>
                            <li class="nav-item"><a href="#solicitacao-presencial" class="nav-link" id="scrollLink" style="color:rgb(0,0,0);">Solicitação Presencial</a></li>
                            <a href="{{ route('graficos') }}" class="nav-link" style="color:rgb(0,0,0);"> <li class="nav-item">Gráficos e Estatísticas</li></a>
                            <li class="nav-item" id="efeito">
                                <a href="#" class="nav-link" id="scrollLink"  style="color:rgb(0,0,0);">
                                    LAI
                                </a>
                                <div class="submenu">
                                    <a href="{{ asset('/register') }}"><div class="item">e-SIC - Pedido de Acesso a Informação</div></a>
                                    <a href="#"><div class="item">Lei de Acesso A Informação Municipal</div></a>
                                    <a href="http://www.condado.pe.leg.br/perguntas-frequentes-faq/"><div class="item">Perguntas Frequentes</div></a>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="linha-menu" class="col-md-12 menu-2" style="min-height:50px;position: fixed;z-index: 9999;background: #edecec;">
        <div id="menu-central" class="container" >
            <div class="navbar navbar-expand-md navbar-dark bg-dark" style="background-color:#edecec!important;">
                <div class="container" style="color:#000000;"><span class="text-white d-md-none" style="color:rgb(0,0,0)!important;">e-Sic Câmara de Machados</span><button class="btn btn-link navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav"><i class="fa fa-align-justify" style="color:rgba(0,0,0,0.5);"></i></button>
                    <div id="main-nav" class="navbar-collapse collapse">
                        <ul class="navbar-nav nav-fill w-100">
                            <li class="nav-item"><a href="{{ url('/') }}" class="nav-link"  style="color:rgb(0,0,0);">Home</a></li>
                            <li class="nav-item"><a href="#como" class="nav-link" id="scrollLink" style="color:rgb(0,0,0);">Como Solicitar Uma Informação</a></li>
                            <li class="nav-item"><a href="#solicitacao-presencial" class="nav-link" id="scrollLink" style="color:rgb(0,0,0);">Solicitação Presencial</a></li>
                            <a href="{{ route('graficos') }}" class="nav-link" style="color:rgb(0,0,0);"> <li class="nav-item">Gráficos e Estatísticas</li></a>
                            <li class="nav-item" id="efeito">
                                <a href="#" class="nav-link" id="scrollLink"  style="color:rgb(0,0,0);">
                                    LAI
                                </a>
                                <div class="submenu">
                                    <a href="{{ asset('/register') }}"><div class="item">e-SIC - Pedido de Acesso a Informação</div></a>
                                    <a href="#"><div class="item">Lei de Acesso A Informação Municipal</div></a>
                                    <a href="#"><div class="item">Perguntas Frequentes</div></a>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    @yield('content')
    <div class="footer-clean" style="background-color:rgb(1,1,1);color:rgb(250,252,255);">
        <footer>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-sm-12 col-md-12" style="position: relative;float: left;">
                        <div class="col-md-3" style="margin: 0 auto;overflow: hidden;">
                            <img src="{{ asset('img/logomarca-condado.png') }}" style="position:sticky;top:10px;width: 250px;">
                        </div>
                    </div>
                    <div class="col-lg-12" style="position: relative;float: left;text-align: center">
                        <p class="copyright">Todos os Direitos Reservados a Câmara Municipal do Condado-PE © {{ date('Y') }} Desenvolvido por
                            <a href="http://prodatta.com">Prodatta</a></p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="{{ asset('principal/assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('principal/assets/bootstrap/js/bootstrap.min.js') }}"></script>

    <script>

        $(document).ready(function(){

            $( "a#scrollLink" ).click(function( event ) {

                event.preventDefault();

                $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 1500);

            });

        });

    </script>
<script type="text/javascript">
    $(function(){
        $( ".menu-2" ).hide();
    });
    $( window ).scroll(function() {
        nScrollPosition = $( window ).scrollTop();
        if(nScrollPosition>=240){
            $( ".menu-2" ).fadeIn("slow");
        }else{
            $( ".menu-2" ).fadeOut( "fast");
        }
    });

</script>
</body>



</html>
