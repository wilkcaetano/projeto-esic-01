<?php

namespace App\Http\Controllers;

use App\Mail\envioEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class appController extends Controller
{
    public function index()
    {
        return view ('index.teste');
    }

    public function enviarEmail()
    {
        //enviar o primeiro email
        Mail::to('wilk.caetano@gmail.com')->send(new envioEmail());

    }
}
