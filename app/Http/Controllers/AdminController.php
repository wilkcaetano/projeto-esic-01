<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Chat;
use App\Solicitacao;
use App\User;
use Dotenv\Validator;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class AdminController extends Controller
{
    public function __construct ()
    {
        //$this->middleware('auth');
    }
    //Redireciona para primeira página do painel de controle do ADM
    public function index()
    {
        $solicitacao = Solicitacao::all();
        return view('admin.index', compact('solicitacao'));
    }

    //Redireciona Para a pagina de login do ADM
    public function login()
    {
        return view('auth.login-adm');
    }

    //Criar novo administrador
    public function postLogin(Request $request)
    {
        $validator = validator($request->all(),[
            'email' => 'required|min:3|max:100',
            'password' => 'required|min:3|max:100',
        ]);


        if($validator->fails()){
            return redirect('/admin/login')
                ->withErrors($validator)
                ->withInput();
        }
        $credenciais = ['email' =>$request->get('email'), 'password' =>$request->get('password') ];

        if (auth()->guard('admin')->attempt($credenciais))
        {
            return redirect('/admin');
        }else{
            return redirect('/admin/login')->withErrors(['error' => 'Login ou senha inválidos'])->withInput();
        }
    }

    //Fazer Logout
    public function logout()
    {
        auth()->guard('admin')->logout();
        return redirect('/admin/login');
    }


    //Para Visualizar Solicitações Recentes
    public function recentes()
    {
        $solicitacao  = Solicitacao::all();
        $users = User::all();
        return view('admin.recentes', compact('solicitacao','users'));
    }


    //Para Visualizar Solicitações Abertas
    public function abertas()
    {
        $solicitacao  = Solicitacao::all();
        $users = User::all();
        return view('admin.abertas', compact('solicitacao', 'users'));
    }

    //Mostrar Usuários Solicitantes Cadastrados no sistema
    public function usuarios()
    {
        $user = User::all();
        //$solicitacao = Solicitacao::all();
           return view('admin.user', compact('user'));

        //return view('admin.user', compact('user'));
    }


    //Mostrar pagina para escolher deletar ou não o solicitante
    public function delete($id)
    {
        $user = User::find($id);
        return view('admin/user_delete', compact('user'));
    }


    //Deletar Usuários Solicitante
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('usuarios');
    }


    //Pagina para cadastrar novos ADM ou editor no sistema
    public function cadastroadm()
    {
        return view('admin.cadastro_adm');
    }


    //Codigo de Criação de novos ADM's ou Editores no sistema
    public function createadm(Request $request)
    {
        try {
            $dados = $request->all();
            $nome = $dados['name'];
            $email = $dados['email'];
            $senha = $dados['password'];
            $cargo = $dados['cargo'];
            $dados['password'] = Hash::make($dados['password']);
            Admin::create($dados);
            Mail::send('mail.cadastro-adm', ['nome' => $nome, 'senha' => $senha, 'email' => $email, 'cargo' => $cargo], function ( $message ) use ( $email ) {
                $message->subject('Você foi cadastrado  na plataforma e-SIC');
                $message->to($email);
            });
            $admin = Admin::all();
            return view('admin.view_adm', compact('admin'));
        }
        catch (\PDOException $e)
        {
            return redirect()->back()->with('message','Cadastro Não Realizado, provavelmente existe dois emails ou nomes iguais');
        }
    }


    //Visualização de novos administradores
    public function view()
    {
        $admin = Admin::all();
        return view('admin.view_adm', compact('admin'));
    }


    //Pagina lista completa de adminstradores
    public function maisinformacoes($id)
    {
        $user = User::find($id);
        return view('admin.info_userfim', compact('user'));
    }

    //Pagina detalhada de informação dos admnistradores do sistema
    public function infoadmin($id)
    {
        $adm = Admin::find($id);
        return view('admin.info-adm', compact('adm'));
    }

    //Redireciona para página de delet
    public function viewdelet($id)
    {
        $adm = Admin::find($id);
        return view('admin.delet-adm', compact('adm'));
    }

    //Deletar Adm e Editor
    public function destroiadm($id)
    {
        Admin::find($id)->delete();
        $admin = Admin::all();
        return view('admin.view_adm', compact('admin'));
    }


}
