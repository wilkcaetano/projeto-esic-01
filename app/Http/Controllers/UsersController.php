<?php

namespace App\Http\Controllers;

use App\Solicitacao;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class UsersController extends Controller
{
    public function index(){
        return view('users.conteudo1');
    }
    public function conteudo(){
        return view('users.conteudo2');
    }
    protected function create(Request $request)
    {

        $dados = $request->all();
        $senha = $dados['password'];
        $dados['password'] = Hash::make($dados['password']);
        $nome = $dados['name'];
        $email = $dados['email'];
        User::create($dados);
        Mail::send('mail.cadastro', ['nome' => $nome, 'senha' => $senha, 'email' => $email], function($message) use($email) {
            $message->subject('Cadastro Realizado na plataforma e-SIC');
            $message->to($email);
        });
        return redirect(route('login'))->with('message', 'Cadastro Realizado com Sucesso entre com seu email e senha para acessar o painel!');
    }

}
