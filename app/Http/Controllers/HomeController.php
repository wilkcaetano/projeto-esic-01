<?php

namespace App\Http\Controllers;

use App\Resposta;
use App\Solicitacao;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitacao =  Solicitacao::all();
        $resposta = Resposta::all();
        return view('painel.homepage', compact('solicitacao', 'resposta'));
    }
}
