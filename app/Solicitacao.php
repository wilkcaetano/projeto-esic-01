<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitacao extends Model
{
    protected $fillable = [
        'id_users',  'prioridade', 'tipo', 'mensagem', 'status', 'arquivo',
    ];
}
